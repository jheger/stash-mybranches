/*
 * This is inspired by the plugin 'stash-inbox'.
 */
define('jh/atlassian/stash/mybranches',
    ['@atlassian/aui', 'lodash', 'bitbucket/util/navbuilder'],
    function (AJS, _, navBuilder) {

        const IS_LOGGING = false;
        const KEY_DELIMITER = ':_:__:_:'; // Note: ':' is a valid character in Bitbucket project names.
        const DIALOG_TRIGGER_ID = 'mybranches-dialog-trigger';
        const DIALOG_ID = 'mybranches-dialog';
        const STORAGE_KEY = 'stash-mybranches';
        const POLL_INTERVAL = 1000;
        // colors.less: @ak-color-B400: #0052CC; E. g. font color.
        const ATLASSIAN_BLUE = '#0052CC';
        const TOOLTIP_HELPTEXT_DELAY_IN = 1000;
        // Tooltip is up-left.
        const DEFAULT_TOOLTIP_GRAVITY = 'se';
        // Buttons for Reload and Delete: Tooltip is left-down.
        const BUTTON_TOOLTIP_GRAVITY = 'ne';
        const storage = window['localStorage']; // Assume all browsers support storage in the meantime
        var $gDialogContent;
        var gProgressValue = 0;
        var gIsError = false;


        /**
         * Set a flag at login
         */
        if (window.location.pathname.match(/\/login$/)) {
            log('window.location.pathname.match login');
            storage.removeItem(STORAGE_KEY);
        }

        // Use this regex to remove the time stamp from logfile:
        // ^\d\d\d\d-\d\d-\d\d\s\d\d:\d\d:\d\d,\d\d\d
        function log(message) {
            if (IS_LOGGING) {
                AJS.log(moment().format('YYYY-MM-DD HH:mm:ss,SSS') + ' My Branches ' + message);
            }
        }

        function logAlways(message) {
            AJS.log(message);
        }

        var handleDialogError = function ($content, response) {
            var defaultError = {
                title: AJS.I18n.getText('jh.atlassian.stash.mybranches.error.title'),
                message: AJS.I18n.getText('jh.atlassian.stash.mybranches.error.unknown')
            };

            var responseError = {};
            if (response) {
                responseError = response.errors ? response.errors[0] : response;
            }
            var error = $.extend({}, defaultError, responseError);

            $content.html(AJS.$(bitbucket.internal.widget.focusMessage.error({
                title: error.title,
                text: error.message,
                extraClasses: 'communication-error'
            })));
            return false;
        };

        function createCommitTooltipContent(commitModel) {
            var htmlTooltip = '<div>';
            // displayId transports also the date. The date is separated by colon. Replace the colon by newline.
            htmlTooltip += commitModel.displayId.replace(/,/, '<br/>') + '<br/>';
            htmlTooltip += AJS.escapeHtml(commitModel.authorName.replace(' ', '&nbsp;'))
                + AJS.escapeHtml('&nbsp;&nbsp;&lt;' + commitModel.authorEmailAddress + '&gt;')
                + '<br/>';
            htmlTooltip += AJS.escapeHtml(AJS.escapeHtml(commitModel.subject));
            htmlTooltip += '</div>';

            return htmlTooltip;
        }

        /*
         * Render a set of commits as SVG.
         *
         * The commits are filled circles.
         *
         * Each single commit-symbol consists of a leading rectangle, a circle, a tooltip, and an optional branch 'nozzle' in case that commit is a merge-commit.
         *
         * In case of a merge-commit, a height of 30 is needed. The circle's bottom reaches down to the 30px, the branch nozzle up to the 0px (approx! Most position
         * values got by try and error). If the set doesn't contain any merge-commit, a height of 20 is sufficient.
         *
         */
        function renderCommitGraph(branchModel, maxCommits) {

            var commitModels = branchModel.commitModels;

            // height of 16 and 30 is to vertical align the SVG properly.
            // Height of 30 is needed for a commit graph including the branch 'nozzle'. In this case, the full height is used.
            // In case of no branch, height of 16 is needed for a commit graph without branch 'nozzle'.
            var height = 16;
            var tooltipDivYOffset = 0;
            // All y values are calculated for SVG including branch nozzle. If absent, shift up all elements.
            var transformY = -9;
            var commitCount = commitModels.length;
            var isDoTransformY = false;
            AJS.$.each(commitModels, function (index, commitModel) {
                if (commitModel.parentCount > 1) {
                    isDoTransformY = true;
                    return false;
                }
            });

            if (isDoTransformY) {
                height = 30;
                transformY = 0;
                tooltipDivYOffset = 9;
            }

            var dBetweenCircles = 34;
            var transformX = (maxCommits - commitCount) * dBetweenCircles;
            var transform = 'transform="translate(' + transformX + ',' + transformY + ')"';
            var color = ATLASSIAN_BLUE;
            var lines = [];
            // Combining tipsy tooltip with SVG is not possible. So we need a workaround here: Each commit circle is overlaid by a div, and that div
            // has the tooltip text.
            var tooltipDivs = [];
            lines.push('<div class="mybranches-commits-block">');
            svgWidth = 22 + (maxCommits - 1) * dBetweenCircles + 6;
            lines.push('<svg width="' + svgWidth + '" height="' + height + '" rxmlns="http://www.w3.org/2000/svg">');
            lines.push('<g ' + transform + '>');
            var rectX = commitCount < maxCommits ? 22 : 0;
            var rectWidth = svgWidth - transformX - rectX - 3;
            lines.push('<rect x="' + rectX + '" y="18" width="' + rectWidth + '" height="2" style="fill:' + color + '; stroke-width:0px"/>');

            AJS.$.each(commitModels, function (index, commitModel) {

                var x = 22 + index * dBetweenCircles;
                if (commitModel.parentCount > 1) {
                    // Commit is merge, add the branch 'nozzle'
                    var dx1 = dBetweenCircles / -4;
                    var dy1 = 0;
                    var dx2 = dBetweenCircles / -4;
                    var dy2 = -14;
                    var dx = dBetweenCircles / -2;
                    var dy = -14;
                    var curveTo = 'm ' + (x - 3) + ',' + 18 + ' c ' + dx1 + ',' + dy1 + ' ' + dx2 + ',' + dy2 + ' ' + dx + ',' + dy;
                    lines.push('<path d="' + curveTo + '" style="fill:none; stroke:' + color + '; stroke-width:2px"/>');
                }

                var r;
                if (commitModels[index].ownCommit) {
                    r = 6;
                } else {
                    r = 4;
                }

                lines.push('<circle fill="' + color + '" cx="' + x + '" cy="19" r="' + r + '" style="stroke:#ffffff; stroke-width:0px">');
                lines.push('</circle>');

                // E.g. /bitbucket/projects/P1/repos/repo-p1-r1/commits/7e574ea6aa4fbaf21eea98291b8c3f98f2ea35be
                var commitUrl = navBuilder.newBuilder().addPathComponents('projects', branchModel.projectKey, 'repos', branchModel.repositorySlug, 'commits',
                    commitModel.id).build();

                // "Elements whose display value was assigned to 'none' will not appear in the tree".
                // See http://www.html5rocks.com/en/tutorials/internals/howbrowserswork/#Render_tree_construction
                // Without display none there is an significant performance drawback at scrolling.
                var a =
                    '<a style="text-decoration: none; display:none" target="_blank" href="'
                    + commitUrl
                    + '"><div class="mybranches-commit-tooltip-area" style="top:'
                    + tooltipDivYOffset
                    + 'px; left:'
                    + (x + transformX - 10)
                    + 'px; width:'
                    + 20
                    + 'px; height:'
                    + 20
                    + 'px" title="'
                    + createCommitTooltipContent(commitModel)
                    + '">&nbsp;</div></a>';
                tooltipDivs.push(a);
            });
            lines.push('</g>');
            lines.push('</svg>');
            lines.push(tooltipDivs.join(''));
            lines.push('</div>');

            return lines.join('');
        }

        function deleteBranchModelFromStore(projectKey, repositorySlug, branchDisplayName) {
            var branchModelsMeta = JSON.parse(storage.getItem(STORAGE_KEY));
            var foundAtIndex = -1;
            AJS.$.each(branchModelsMeta.branchModels, function (index, branchModel) {
                if (branchModel.projectKey === projectKey && branchModel.repositorySlug === repositorySlug && branchModel.branchDisplayName === branchDisplayName) {
                    foundAtIndex = index;
                    return false;
                }
            });

            if (foundAtIndex >= 0) {
                branchModelsMeta.branchModels.splice(foundAtIndex, 1);
                storage.setItem(STORAGE_KEY, JSON.stringify(branchModelsMeta));
                // log('deleteBranchModelFromStore(), after ' + JSON.stringify(branchModels));
            } else {
                logAlways('ERROR: Could not delete branch model ' + projectKey + '/' + repositorySlug + '/' + branchDisplayName + ' form store.');
            }
        }

        function fadeoutDialogAndClickTriggerIfOpen() {
            var $dialog = $('#' + DIALOG_ID);
            var isDialogOpen = $dialog.prop('open');
            log('fadeoutDialogAndClickTriggerIfOpen(), isDialogOpen ' + isDialogOpen);
            if (isDialogOpen) {
                $dialog.fadeOut(function () {
                    // Faded out doesn't mean it's closed. Close it.
                    $dialog.prop('open', false);
                    // ITs more smooth for the eyes waiting some time before opening the dialog again.
                    setTimeout(function () {
                        $('#' + DIALOG_TRIGGER_ID)[0].click();
                    }, 200);
                });
            }
        }

        function registerReload() {
            AJS.$('#mybranches-reload-button').click(function (event) {
                log('#mybranches-reload-button clicked');
                event.preventDefault();
                storage.removeItem(STORAGE_KEY);
                gProgressValue = 0;
                removeBranchCount();
                loadProgressOrDialogResources();
                fadeoutDialogAndClickTriggerIfOpen();
            });
        }

        function registerDelete() {
            AJS.$('#mybranches-submitbutton').click(function (event) {
                event.preventDefault();

                AJS.$('#mybranches-table-wrapper input:checked').each(function (index, checkedBranch) {
                    var tr = AJS.$(checkedBranch).closest('tr');
                    // The table shows the project's name, but we need the project's key here.
                    var projectKey = AJS.$(tr).find('td:nth-child(1)>span:nth-child(1)').attr('project-key');
                    var repositorySlug = AJS.$(tr).find('td:nth-child(2)>span').attr('repository-slug');
                    var branchName = AJS.$(tr).find('td:nth-child(3)').text();
                    var url = AJS.contextPath() + '/rest/branch-utils/1.0/projects/' + projectKey + '/repos/' + repositorySlug + '/branches';
                    var data = '{"name":"refs/heads/' + branchName + '","dryRun":false}';

                    // Note:
                    // From the Atlassian REST branch-utils docs:
                    // If the branch does not exist, this operation will not raise an error. In other words after calling this resource and receiving a 204 response
                    // the branch provided in the request is guaranteed to not exist in the specified repository any more, regardless of its existence beforehand.
                    AJS.$.ajax({
                        type: 'DELETE',
                        url: url,
                        data: data,
                        contentType: 'application/json',
                        cache: false
                    }).done(function () {
                        AJS.$(tr).remove();
                        decrementBranchCount();
                        deleteBranchModelFromStore(projectKey, repositorySlug, branchName);
                        AJS.flag({
                            type: 'success',
                            close: 'auto',
                            title: AJS.I18n.getText('jh.atlassian.stash.mybranches.branchdeleted')
                        });
                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        logAlways('ERROR: branch deletion: ' + JSON.stringify(jqXHR));
                        var message = JSON.parse(jqXHR.responseText).errors[0].message;
                        var details = JSON.parse(jqXHR.responseText).errors[0].details;
                        // Some error details are equal to their messages. In that case, don't bore the user displaying both.
                        if (details !== undefined && details !== message) {
                            // details is an array, normally with a single entry with the details. But technically there could be more than
                            // one detail. Give all of them to the message dialog. But: The message element is somewhat sensible to
                            // the input data type, it expects a String, not an array. By concatenating an empty string to the array,
                            // JS makes it a string.
                            details += '';
                        }
                        // Some errors have "message" and "details", some have only a "message".
                        //  - The error "You cannot delete this branch since it has at least one open pull request
                        //      associated with it" only has the "message".
                        //  - The error
                        //          "Branch 'breakit/BREAKIT-118-increase-httpcomponents-version' could not be deleted"
                        //      has the 'message' and the 'details'
                        //          "'refs/heads/breakit/BREAKIT-118-increase-httpcomponents-version' is read-only.\n
                        //          Check your branch permissions configuration with the project administrator."
                        AJS.flag({
                            type: 'error',
                            // close: 'manual' is the default. But just for documenting.
                            close: 'manual',
                            title: message,
                            body: details
                        });
                    })
                });
            });
        }

        // Without destroy, the tooltip might pop up after My Branches list closes and progress page displays:
        // If the user hover the refresh button and clicks it before the 1s delay, the progress dialog shows, and after the 1s delay the
        // refresh-button's tooltip, too.
        // Unfortunately, there is - in my experience - no way to stop a tooptip from popping up after triggered.
        // Assure the refresh tooltip is removed latest when the My Branches dialog closes, or the progress dialog shows.
        function unbindAndRemoveRefreshButtonTooltip() {
            AJS.$('#mybranches-reload-button').tooltip('destroy');
        }

        function updateProgressIndicator() {
            log('updateProgressIndicator(), gProgressValue ' + gProgressValue);
            if ($('#mybranches-progress').length > 0) {
                AJS.progressBars.update('#mybranches-progress', gProgressValue);
            }
        }

        function getI18nPullRequestState(state) {
            var i18nState = 'unknown state';
            if (state === 'OPEN') {
                i18nState = AJS.I18n.getText('bitbucket.web.pullrequest.state.open');
            } else if (state === 'MERGED') {
                i18nState = AJS.I18n.getText('bitbucket.web.pullrequest.state.merged');
            } else if (state === 'DECLINED') {
                i18nState = AJS.I18n.getText('bitbucket.web.pullrequest.state.declined');
            }

            return i18nState;
        }

        function activateTooltips(commitsBlock) {
            var $a = AJS.$(commitsBlock).find('a');
            $a.css('display', 'block');
            AJS.$(commitsBlock).find('.mybranches-commit-tooltip-area').tooltip({
                gravity: DEFAULT_TOOLTIP_GRAVITY,
                html: true,
                delayIn: 0,
                delayOut: 0
            });
        }

        /**
         * Activate tooltips in viewport only.
         *
         * Tooltips causes a significant performance impact on large amount of branches.
         *
         * @param commitsBlock
         */
        function activateTooltipsInViewport(commitsBlock) {
            var bounds = commitsBlock.getBoundingClientRect();
            var $a = AJS.$(commitsBlock).find('a');
            if (bounds.top < window.innerHeight && bounds.bottom > 0) {
                activateTooltips(commitsBlock);
            } else {
                $a.css('display', 'none');
                //AJS.$(this).find('.mybranches-commit-tooltip-area').tipsy('destroy');
                // Workaround because tooltip('destroy') doesn't work:
                AJS.$(commitsBlock).find('.mybranches-commit-tooltip-area').unbind('mouseenter mouseleave');
            }
        }

        function renderBranchesDialog(branchModelsMeta) {
            // Create pull request info
            var branchNameToPullRequestMap = {};
            AJS.$.each(branchModelsMeta.branchModels, function (index, branchModel) {
                var key = branchModel.projectKey + KEY_DELIMITER + branchModel.repositorySlug + KEY_DELIMITER + branchModel.branchDisplayName;
                if (branchModel.pullRequestModels.length > 0) {
                    // rules="rows": Display only the borders between the rows:
                    var pullRequestTitle = '<table rules="rows" class="mybranches-pullrequest-tooltip"><tbody>';
                    AJS.$.each(branchModel.pullRequestModels, function (index, pullRequestModel) {
                        pullRequestTitle += '<tr>';
                        pullRequestTitle += '<td>#&nbsp;' + pullRequestModel.id + '</td>';
                        pullRequestTitle += '<td>' + getI18nPullRequestState(pullRequestModel.state) + '</td>';
                        pullRequestTitle += '<td>' + AJS.escapeHtml(pullRequestModel.title) + '</td>';
                        pullRequestTitle += '</tr>';
                    });
                    pullRequestTitle += '</tbody></table>';
                    branchNameToPullRequestMap[key] = [branchModel.displayPullReqestsState, pullRequestTitle];
                }
            });

            // Create branch URLs
            AJS.$.each(branchModelsMeta.branchModels, function (index, branchModel) {
                // E.g. /bitbucket/projects/P1/repos/repo-p1-r1/commits?until=refs%2Fheads%2FIDE-Connectors
                branchModel.branchUrl = navBuilder.newBuilder().addPathComponents('projects', branchModel.projectKey, 'repos', branchModel.repositorySlug,
                    'commits').withParams({
                    until: 'refs/heads/' + branchModel.branchDisplayName
                }).build();
            });

            var $dialog = AJS.$(jh.atlassian.stash.mybranches.branchesDialog({
                branchModels: branchModelsMeta.branchModels,
                branchNameToPullRequestMap: branchNameToPullRequestMap,
                keyDelimiter: KEY_DELIMITER
            }));

            AJS.$.each(branchModelsMeta.branchModels, function (index, branchModel) {
                var commitGraphHtml = renderCommitGraph(branchModel, branchModelsMeta.maxCommits);
                $dialog.find('#mybranches-graph-' + index).append(commitGraphHtml);
            });

            var i = 0;
            // Unfortunately there is no event in case the dialog rendering completes, so here we can't calculate which columns are visible initially.
            // So simply show the first 20 ones.
            $dialog.find('.mybranches-commits-block').each(function () {
                i++;
                if (i > 20) {
                    return false;
                }
                activateTooltips(this);
            });

            $dialog.find('#mybranches-table-wrapper').on('scroll', _.debounce(
                function () {
                    $dialog.find('.mybranches-commits-block').each(function () {
                        activateTooltipsInViewport(this);
                    });
                },
                250));

            // Project-icon- and repo-name column
            $dialog.find('#mybranches-table-wrapper').find('td').filter(':nth-child(1), :nth-child(2)').find('span').tooltip({
                gravity: DEFAULT_TOOLTIP_GRAVITY,
                delayIn: 0,
                delayOut: 0
            });

            $dialog.find('.mybranches-pullrequests-status').find('span').tooltip({
                gravity: DEFAULT_TOOLTIP_GRAVITY,
                html: true,
                delayIn: 0,
                delayOut: 0
            });

            $dialog.find('#mybranches-reload-button').tooltip({
                title: function () {
                    return AJS.I18n.getText('jh.atlassian.stash.mybranches.refreshButton');
                },
                gravity: BUTTON_TOOLTIP_GRAVITY,
                html: true,
                delayIn: TOOLTIP_HELPTEXT_DELAY_IN,
                delayOut: 0
            });
            $dialog.find('#mybranches-submitbutton').tooltip({
                title: function () {
                    return AJS.I18n.getText('jh.atlassian.stash.mybranches.deleteButton');
                },
                gravity: BUTTON_TOOLTIP_GRAVITY,
                html: true,
                delayIn: TOOLTIP_HELPTEXT_DELAY_IN,
                delayOut: 0
            });

            return $dialog;
        }

        function onShow() {
            log('onShow()');

            var $dialog = $('#' + DIALOG_ID);

            if (gIsError) {
                log('onShow(), is error');
                $dialog.empty().append(AJS.$(jh.atlassian.stash.mybranches.errorDialog()));
                return;
            }

            var branchModelsMetaJson = storage.getItem(STORAGE_KEY);

            if (branchModelsMetaJson === null) {
                log('onShow(), branchModelsMetaJson === null');
                unbindAndRemoveRefreshButtonTooltip();
                $dialog.empty().append(AJS.$(jh.atlassian.stash.mybranches.progressDialog()));
                AJS.progressBars.update('#mybranches-progress', gProgressValue);
                return;
            }

            var $dialogContent;
            var branchModelsMeta = JSON.parse(branchModelsMetaJson);
            if (branchModelsMeta.branchModels.length > 0) {
                $dialogContent = renderBranchesDialog(branchModelsMeta);
            } else {
                $dialogContent = AJS.$(jh.atlassian.stash.mybranches.emptyDialog({
                    adminRoleExcluded: branchModelsMeta.adminRoleExcluded,
                    maxCommits: branchModelsMeta.maxCommits
                }));
            }
            $dialog.empty().append($dialogContent);

            registerReload();
            registerDelete();
        }

        function decrementBranchCount() {
            var $badge = $('#' + DIALOG_TRIGGER_ID + '>aui-badge');
            $badge.text($badge.text() - 1);
        }

        function addBranchCount(count) {
            var $badge = $(aui.badges.badge({
                'text': count
            }));
            AJS.$('#' + DIALOG_TRIGGER_ID).append($badge);
            $badge.addClass('visible');
        }

        function removeBranchCount() {
            AJS.$('#' + DIALOG_TRIGGER_ID + '>aui-badge').remove();
        }

        function loadProgressOrDialogResources() {
            log('loadProgressOrDialogResources()');

            AJS.$.ajax({
                url: AJS.contextPath() + '/rest/mybranches/1.0/branches',
                dataType: 'json',
                cache: false
            }).statusCode({
                200: function (progressValue) {// OK: In progress
                    log('loadProgressOrDialogResources 200, progress ' + progressValue);
                    gProgressValue = progressValue;
                    updateProgressIndicator();
                    setTimeout(loadProgressOrDialogResources, POLL_INTERVAL);
                },
                201: function (branchModelsMeta) {// Created: Got branch models
                    log('loadProgressOrDialogResources 201 done, branch count: '
                        + branchModelsMeta.branchModels.length + ', maxCommits ' + branchModelsMeta.maxCommits);
                    storage.setItem(STORAGE_KEY, JSON.stringify(branchModelsMeta));
                    // log('loadProgressOrDialogResources 201 done, branches: ' + JSON.stringify(branchModels));
                    addBranchCount(branchModelsMeta.branchModels.length);
                    fadeoutDialogAndClickTriggerIfOpen();
                },
                202: // Accepted: Server will create branch models
                    function () {
                        log('loadProgressOrDialogResources 202');
                        setTimeout(loadProgressOrDialogResources, 100); // Immediately get first progress value
                    }
            }).fail(function (jqXHR, textStatus, errorThrown) {
                log('loadProgressOrDialogResources fail');
                // handleDialogError($content);
                gIsError = true;
                fadeoutDialogAndClickTriggerIfOpen();
            }).always(function () {
            });
        }


        log('onReady');

        // TODO Move to admin context
        AJS.$('.mybranches-admin-config-option').find('div.error').each(function (index, element) {
            var html = AJS.$(element).text();
            log('#### html ' + html);
            AJS.$(element).text('');
            // Error message is taken from the PatternSyntaxException and shall be show formatted. But:
            // Original div is <div class="error">This is my error message</div>, but class 'error' removes my <pre>.
            // So replace original div with mine without class 'error'.
            AJS.$(element).replaceWith('<div style="color: #d04437">' + html + '</div>');
        });

        // TODO Move to admin context
        // TODO Add word 'admin' to cancel button id.
        AJS.$('#mybranches_cancel_button').attr('href', AJS.contextPath() + '/admin');

        var $dialogTriggerElem = AJS.$('#' + DIALOG_TRIGGER_ID);
        if ($dialogTriggerElem.length) {
            $dialogTriggerElem.html(jh.atlassian.stash.mybranches.triggerIcon());
            var $dialogTriggerElemWithAuiTriggerAttributes = $dialogTriggerElem.clone(true, true);
            $dialogTriggerElemWithAuiTriggerAttributes.attr('data-aui-trigger', '');
            $dialogTriggerElemWithAuiTriggerAttributes.attr('aria-controls', DIALOG_ID);
            $dialogTriggerElem.replaceWith($dialogTriggerElemWithAuiTriggerAttributes);

            // Create empty dialog.
            $('body').append('<aui-inline-dialog id="' + DIALOG_ID + '"'
                + ' alignment="bottom right" class="mybranches-aui-inline-dialog">'
                + '</aui-inline-dialog>');


            var $dialog = $('#' + DIALOG_ID);
            $dialog.on('aui-show', function () {
                log('EVT on aui-show');
                onShow();
            });

            var branchModelsMetaJson = storage.getItem(STORAGE_KEY);
            if (branchModelsMetaJson === null) {
                loadProgressOrDialogResources(); // Initiate or continue loading
            } else {
                try {
                    var branchModelsMeta = JSON.parse(branchModelsMetaJson);
                    // If the format is unknown (in case of updates), load data from server
                    if (branchModelsMeta.branchModels === undefined || branchModelsMeta.maxCommits === undefined) {
                        loadProgressOrDialogResources();
                    } else {
                        addBranchCount(JSON.parse(branchModelsMetaJson).branchModels.length);
                    }
                } catch (e) {
                    loadProgressOrDialogResources();
                }
            }
        }

    });


AJS.toInit(function () {
    require('jh/atlassian/stash/mybranches');
});
