package jh.atlassian.stash.mybranches;

import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.user.ApplicationUser;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Future;

class BranchModelsMeta {
    // Set initial progress to this value. Without this, initial progress might be 0 for the first JS POLL_INTERVAL.
    private static final double INITIAL_PROGRESS = 0.05;
    // Set progress of second to last repository to this value. Without this, that progress might be e. g. 75% and jumping to branch dialog.
    // I think the user expect something near 100% just before finishing.
    private static final double FINAL_PROGRESS = 0.95;
    private List<Repository> repositories = Collections.emptyList();
    private int repositoryProgress = 0;
    private ApplicationUser applicationUser;
    @JsonProperty
    private List<BranchModel> branchModels = new ArrayList<>();
    private long resourceCreatedTime = 0; // 0 means 'in progress'
    private Future<List<BranchModel>> future = null;
    @JsonProperty
    private int maxCommits;
    @JsonProperty
    private boolean adminRoleExcluded;
    @JsonProperty
    private String emailAdress;
    @JsonProperty
    private String originalEmailAdress;


    public BranchModelsMeta(ApplicationUser applicationUser) {
        this.applicationUser = applicationUser;
        this.originalEmailAdress = applicationUser.getEmailAddress();
    }

    public ApplicationUser getApplicationUser() {
        return applicationUser;
    }

    public void setRepositories(List<Repository> repositories) {
        this.repositories = repositories;
    }

    public List<Repository> getRepositories() {
        return repositories;
    }

    public synchronized void incRepositoryProgress() {
        this.repositoryProgress += 1;
    }

    public synchronized int getRepositoryProgress() {
        return repositoryProgress;
    }

    public synchronized double getProgress() {
        double progress = (double) repositoryProgress / repositories.size();
        progress += INITIAL_PROGRESS;
        if ((repositoryProgress == repositories.size() - 1) && progress < FINAL_PROGRESS) {
            progress = FINAL_PROGRESS;
        }
        return progress;
    }

    public synchronized void addBranchModels(List<BranchModel> branchModels) {
        this.branchModels.addAll(branchModels);
    }

    public synchronized List<BranchModel> getBranchModels() {
        return branchModels;
    }

    public synchronized void setResourceCreatedTime() {
        resourceCreatedTime = new Date().getTime();
    }

    public synchronized long getResourceCreatedTime() {
        return resourceCreatedTime;
    }

    public synchronized Future<List<BranchModel>> getFuture() {
        return future;
    }

    public synchronized void setFuture(Future<List<BranchModel>> future) {
        this.future = future;
    }

    public synchronized int getMaxCommits() {
        return maxCommits;
    }

    public synchronized void setMaxCommits(int maxCommits) {
        this.maxCommits = maxCommits;
    }

    public synchronized boolean isAdminRoleExcluded() {
        return adminRoleExcluded;
    }

    public synchronized void setAdminRoleExcluded(boolean adminRoleExcluded) {
        this.adminRoleExcluded = adminRoleExcluded;
    }

    public void setEmailAdress(String emailAdress) {
        this.emailAdress = emailAdress;
    }

    public String getEmailAddress() {
        return emailAdress;
    }


    @Override
    public String toString() {
        return "[repositoryProgress " + repositoryProgress + ", resourceCreatedDate " + new Date(resourceCreatedTime) + ", branchModels " + branchModels.size
                () + "]";
    }
}
