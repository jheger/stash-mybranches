package jh.atlassian.stash.mybranches;

import com.atlassian.activeobjects.tx.Transactional;

import java.io.IOException;
import java.util.List;

@Transactional
public interface AdminConfigService {
    int MAX_COMMITS_DEFAULT = 4;
    String LIMIT_ADMIN_ROLE_TO_FIND_PERSONAL_FORKS = "personalforks";   // Default
    String LIMIT_ADMIN_ROLE_TO_FIND_NONE = "none";

    /**
     * Get the admin configuration. Because there is only one configuration, no parameter to identify the configuration is needed.
     *
     * @return The admin configuration.
     */
    AdminConfig getAdminConfig();

    void setAlternativeEmailAddressesConfig(String alternativeEmailAddressesConfig);

    String getAlternativeEmailAddressesConfig();

    List<String> getCurrentAndAlternativeEmailAdresses(String forEmailAddress);
}
