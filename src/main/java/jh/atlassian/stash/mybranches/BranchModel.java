package jh.atlassian.stash.mybranches;

import com.atlassian.bitbucket.pull.PullRequest;
import com.atlassian.bitbucket.pull.PullRequestState;
import com.atlassian.bitbucket.repository.Branch;
import com.atlassian.bitbucket.repository.Repository;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

@JsonSerialize
public class BranchModel implements Comparable<BranchModel> {
    private static final Logger log = LoggerFactory.getLogger(BranchModel.class);
    private Branch branch;
    @JsonProperty
    private String branchDisplayName;
    @JsonProperty
    private boolean isAllowDelete = false;
    @JsonProperty
    private String repositoryName;
    @JsonProperty
    private String repositorySlug;
    @JsonProperty
    private String projectKey;
    @JsonProperty
    private String projectName;
    @JsonProperty
    private String projectAvatarUrl;
    @JsonProperty
    private List<CommitModel> commitModels;
    @JsonProperty
    private List<PullRequestModel> pullRequestModels = new ArrayList<BranchModel.PullRequestModel>();
    @JsonProperty
    private String displayPullReqestsState;

    public BranchModel(Branch branch, Repository repository) {
        this.branch = branch;
        this.branchDisplayName = branch.getId().replaceFirst("^refs/heads/", "");
        this.repositoryName = repository.getName();
        this.repositorySlug = repository.getSlug();
        this.projectKey = repository.getProject().getKey();
        this.projectName = repository.getProject().getName();
    }

    public void setProjectAvatarUrl(String projectAvatarUrl) {
        this.projectAvatarUrl = projectAvatarUrl;
    }

    public void setAllowDelete(boolean isAllowDelete) {
        this.isAllowDelete = branch.getIsDefault() ? false : isAllowDelete;
    }

    public void setPullRequests(List<PullRequest> pullRequests) {
        Set<PullRequestState> pullRequestStates = new HashSet<>();
        for (PullRequest pullRequest : pullRequests) {
            pullRequestModels.add(new PullRequestModel(pullRequest));
            pullRequestStates.add(pullRequest.getState());
        }
        if (pullRequestStates.contains(PullRequestState.OPEN)) {
            displayPullReqestsState = PullRequestState.OPEN.name();
        } else if (pullRequestStates.contains(PullRequestState.MERGED)) {
            displayPullReqestsState = PullRequestState.MERGED.name();
        } else if (pullRequestStates.contains(PullRequestState.DECLINED)) {
            displayPullReqestsState = PullRequestState.DECLINED.name();
        }
    }

    public void setCommitModels(List<CommitModel> commitModels) {
        this.commitModels = commitModels;
        Collections.reverse(this.commitModels);
    }

    public Branch getBranch() {
        return branch;
    }

    @Override
    public String toString() {
        return "[projectKey " + projectKey + ", repositoryName " + repositoryName + ", branchDisplayName " + branchDisplayName + ", isAllowDelete " +
                isAllowDelete + "]";
    }

    @Override
    public int compareTo(BranchModel that) {
        int p = this.projectName.compareTo(that.projectName);
        if (p == 0) {
            int r = this.repositoryName.compareTo(that.repositoryName);
            if (r == 0) {
                return this.branchDisplayName.compareTo(that.branchDisplayName);
            } else {
                return r;
            }
        } else {
            return p;
        }
    }

    public static class CommitModel {
        @JsonProperty
        private String id;
        @JsonProperty
        private String displayId;
        @JsonProperty
        private String authorName;
        @JsonProperty
        private String authorEmailAddress;
        @JsonProperty
        private String subject;
        @JsonProperty
        private int parentCount;
        @JsonProperty
        private boolean ownCommit;

        public CommitModel(String id, String displayId, String name, String mailAddress, String subject, int parentCount) {
            this.authorName = name;
            this.authorEmailAddress = mailAddress;
            this.displayId = displayId;
            this.id = id;
            this.subject = subject;
            this.parentCount = parentCount;
        }

        public String getId() {
            return id;
        }

        public String getDisplayId() {
            return displayId;
        }

        public String getAuthorName() {
            return authorName;
        }

        public String getAuthorEmailAddress() {
            return authorEmailAddress;
        }

        public String getSubject() {
            return subject;
        }

        public int getParentCount() {
            return parentCount;
        }

        public boolean isOwnCommit() {
            return ownCommit;
        }

        public void setOwnCommit(boolean isOwnCommit) {
            this.ownCommit = isOwnCommit;
        }

        @Override
        public String toString() {
            return "[displayId " + displayId + ", authorEmailAddress " + authorEmailAddress + ", subject " + subject + ", parentCount " + parentCount + ", " +
                    "ownCommit " + ownCommit;
        }
    }

    public class PullRequestModel {
        @JsonSerialize
        long id;
        @JsonSerialize
        String title;
        @JsonSerialize
        String state;

        PullRequestModel(PullRequest pullRequest) {
            this.id = pullRequest.getId();
            this.title = pullRequest.getTitle();
            this.state = pullRequest.getState().toString();
        }
    }
}
