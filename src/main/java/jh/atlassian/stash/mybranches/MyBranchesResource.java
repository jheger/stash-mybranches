package jh.atlassian.stash.mybranches;

import com.atlassian.bitbucket.auth.AuthenticationContext;
import com.atlassian.bitbucket.avatar.AvatarService;
import com.atlassian.bitbucket.i18n.I18nService;
import com.atlassian.bitbucket.permission.Permission;
import com.atlassian.bitbucket.permission.PermissionService;
import com.atlassian.bitbucket.permission.PermissionValidationService;
import com.atlassian.bitbucket.pull.PullRequestService;
import com.atlassian.bitbucket.repository.RefService;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.RepositoryService;
import com.atlassian.bitbucket.repository.ref.restriction.RefRestrictionService;
import com.atlassian.bitbucket.rest.RestResource;
import com.atlassian.bitbucket.rest.util.RestUtils;
import com.atlassian.bitbucket.scm.git.command.GitCommandBuilderFactory;
import com.atlassian.bitbucket.server.ApplicationPropertiesService;
import com.atlassian.bitbucket.user.ApplicationUser;
import com.atlassian.bitbucket.user.SecurityService;
import com.atlassian.bitbucket.util.Page;
import com.atlassian.bitbucket.util.PageRequest;
import com.atlassian.bitbucket.util.PageRequestImpl;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Path("/")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({RestUtils.APPLICATION_JSON_UTF8})
@AnonymousAllowed
public class MyBranchesResource extends RestResource implements DisposableBean {

    private static final Logger log = LoggerFactory.getLogger(MyBranchesResource.class);
    private static final int RESOURCE_OUTDATED_PERIOD = 5 * 60 * 1000; // 5min
    // Must be static. Otherwise destroy() will not handle it.
    private static final Map<String, BranchModelsMeta> userToBranchModelsInfoMap = new HashMap<>();
    private final AvatarService avatarService;
    private final RefService refService;
    private final RepositoryService repositoryService;
    private final PullRequestService pullRequestService;
    private final PermissionValidationService permissionValidationService;
    private final RefRestrictionService refRestrictionService;
    private final PermissionService permissionService;
    private final SecurityService securityService;
    private final AuthenticationContext authenticationContext;
    private final AdminConfigService adminConfigService;
    private final ApplicationPropertiesService applicationPropertiesService;
    private final GitCommandBuilderFactory gitCommandBuilderFactory;


    public MyBranchesResource(AvatarService avatarService, RefService refService, RepositoryService repositoryService, PullRequestService pullRequestService,
                              PermissionValidationService permissionValidationService, I18nService i18nService, RefRestrictionService refRestrictionService,
                              PermissionService permissionService, SecurityService securityService, AuthenticationContext authenticationContext,
                              AdminConfigService adminConfigService, ApplicationPropertiesService applicationPropertiesService,
                              GitCommandBuilderFactory gitCommandBuilderFactory) {
        super(i18nService);
        this.avatarService = avatarService;
        this.refService = refService;
        this.repositoryService = repositoryService;
        this.pullRequestService = pullRequestService;
        this.permissionValidationService = permissionValidationService;
        this.permissionService = permissionService;
        this.refRestrictionService = refRestrictionService;
        this.securityService = securityService;
        this.authenticationContext = authenticationContext;
        this.adminConfigService = adminConfigService;
        this.applicationPropertiesService = applicationPropertiesService;
        this.gitCommandBuilderFactory = gitCommandBuilderFactory;
    }

    @GET
    @Path("branches")
    public Response getBranches(@Context HttpServletRequest requestContext) {

        permissionValidationService.validateAuthenticated();
        ApplicationUser applicationUser = authenticationContext.getCurrentUser();
        AdminConfig adminConfig = adminConfigService.getAdminConfig();

        String userEmail = applicationUser.getEmailAddress().trim();
        if (!applicationUser.getEmailAddress().equals(userEmail)) {
            log.warn("Email address of Bitbucket user contains leading or trailing spaces: '{}'. My Branches trims email addresses of Bitbucket user as as well as in commits.",
                    applicationUser.getEmailAddress());
        }

        // To treat resources with care, users with admin role might be excluded by configuration. They will get an empty branchModel list.
        if (isAdmin() && adminConfig.getLimitAdminRole() != null && adminConfig.getLimitAdminRole().equals(AdminConfigService.LIMIT_ADMIN_ROLE_TO_FIND_NONE)) {
            BranchModelsMeta branchModelsMeta = new BranchModelsMeta(applicationUser);
            branchModelsMeta.setEmailAdress(userEmail);
            branchModelsMeta.setMaxCommits(adminConfig.getMaxCommits());
            branchModelsMeta.setAdminRoleExcluded(true);
            return Response.status(Response.Status.CREATED).entity(branchModelsMeta).build();
        }

        Response response;
        synchronized (userToBranchModelsInfoMap) {
            BranchModelsMeta branchModelsMeta = userToBranchModelsInfoMap.get(userEmail);
            if (branchModelsMeta == null) {
                log.info("getBranches(), user '{}', branchModelsMeta is null, create resource and return ACCEPTED", userEmail);
                branchModelsMeta = new BranchModelsMeta(applicationUser);
                branchModelsMeta.setEmailAdress(userEmail);
                branchModelsMeta.setMaxCommits(adminConfig.getMaxCommits());
                branchModelsMeta.setRepositories(getRepositories());
                userToBranchModelsInfoMap.put(userEmail, branchModelsMeta);
                startThreads(branchModelsMeta);
                // Check for outdated resources each time a user logs in. This avoids implementing a SAL scheduler.
                // It might be the user logs in and logs out immediately, or closes the browser. In this case the plugin will create the resource, but the
                // browser will not fetch it. So the resource will be hold until this user logs in again (an kept logged in). Removing the resource
                // saves memory.
                deleteOutdatedResources();
                response = Response.status(Response.Status.ACCEPTED).build();
            } else {
                // For each repo a thread is created and each successful finished thread increments repositoryProgress. After all threads are done,
                // ressourceCreatedTime is set. It might be a thread finished by exception. In that case repositoryProgress isn't incremented, but
                // at the end ressourceCreatedTime is set. Or short: If ressourceCreatedTime is set, all threads are done.
                if (branchModelsMeta.getResourceCreatedTime() == 0 && branchModelsMeta.getRepositoryProgress() < branchModelsMeta.getRepositories().size()) {
                    //log.debug("getBranches(), branchModelsIMeta.repositoryProgress {}, return OK", branchModelsMeta.getRepositoryProgress());
                    response = Response.status(Response.Status.OK).entity(branchModelsMeta.getProgress()).build();
                } else {
                    log.info("getBranches(), user '{}', resource created, return CREATED", userEmail);
                    response = Response.status(Response.Status.CREATED).entity(branchModelsMeta).build();
                    userToBranchModelsInfoMap.remove(userEmail);
                }
            }
        }

        return response;
    }

    @GET
    @Path("debug")
    public Response debug(@Context HttpServletRequest requestContext) {

        permissionValidationService.validateAuthenticated();

        synchronized (userToBranchModelsInfoMap) {
            log.warn("userToBranchModelsInfoMap {}", userToBranchModelsInfoMap);
        }

        return Response.status(Response.Status.OK).entity(userToBranchModelsInfoMap).build();
    }

    /*
     * Helper thread to start and wait for the worker thread.
     */
    private void startThreads(final BranchModelsMeta branchModelsMeta) {
        Thread thread = new Thread(new Runnable() {
            String excludeBranchesRegex = adminConfigService.getAdminConfig().getExclBranchRegex();

            @Override
            public void run() {
                ExecutorService threadPool = Executors.newFixedThreadPool(1);
                // Start the worker thread
                branchModelsMeta.setFuture(threadPool.submit(new MyBranchesCollector(applicationPropertiesService, avatarService, permissionService,
                        pullRequestService, refRestrictionService, refService, securityService, gitCommandBuilderFactory, branchModelsMeta, excludeBranchesRegex,
                        adminConfigService.getCurrentAndAlternativeEmailAdresses(branchModelsMeta.getEmailAddress()))));
                List<BranchModel> branchModels;
                try {
                    branchModels = branchModelsMeta.getFuture().get();
                    branchModelsMeta.addBranchModels(branchModels);
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                } finally {
                    threadPool.shutdown();
                    branchModelsMeta.setResourceCreatedTime();
                }
            }
        });
        thread.start();
    }

    private List<Repository> getRepositories() {
        List<Repository> repositories = new ArrayList<>();
        String excludeRepoSRegex = adminConfigService.getAdminConfig().getExclRepoSRegex();
        String excludeProjectNRegex = adminConfigService.getAdminConfig().getExclProjNRegex();
        String excludeProjectKRegex = adminConfigService.getAdminConfig().getExclProjKRegex();
        // The project name of a personal fork is the upper case user name slug with preceding "~".
        // E. g. use name "jörg", user slug "jo?rg", fork "~JO?RG"
        String usersPersonalForkProjectKey = "~" + authenticationContext.getCurrentUser().getSlug().toUpperCase();
        PageRequest repositoryPageRequest = new PageRequestImpl(0, 100);
        while (repositoryPageRequest != null) {
            Page<? extends Repository> pagedRepositories = repositoryService.findAll(repositoryPageRequest);
            for (Repository repository : pagedRepositories.getValues()) {
                if (excludeRepoSRegex != null && repository.getSlug().matches(excludeRepoSRegex)) {
                    continue;
                }
                if (excludeProjectNRegex != null && repository.getProject().getName().matches(excludeProjectNRegex)) {
                    continue;
                }
                if (excludeProjectKRegex != null && repository.getProject().getKey().matches(excludeProjectKRegex)) {
                    continue;
                }
                // Currently adminConfig.getLimitAdminRole() is either LIMIT_ADMIN_ROLE_TO_FIND_NONE or LIMIT_ADMIN_ROLE_TO_FIND_PERSONAL_FORKS.
                // LIMIT_ADMIN_ROLE_TO_FIND_NONE is catched before, so here is only LIMIT_ADMIN_ROLE_TO_FIND_PERSONAL_FORKS left. This is not
                // checked here, because this value must be implicit at this point.
                if (isAdmin() && isAnyPersonalFork(repository.getProject().getKey()) && !repository.getProject().getKey().equals(usersPersonalForkProjectKey)) {
                    continue;   // Skip other's personal forks
                }
                repositories.add(repository);
            }
            repositoryPageRequest = pagedRepositories.getNextPageRequest();
        }
        return repositories;
    }

    private boolean isAdmin() {
        return permissionService.hasGlobalPermission(Permission.SYS_ADMIN) || permissionService.hasGlobalPermission(Permission.ADMIN);
    }

    private boolean isAnyPersonalFork(String projectKey) {
        return projectKey.startsWith("~");
    }

    /**
     * Call this only in synchronized block
     */
    private void deleteOutdatedResources() {
        long currentTime = new Date().getTime();
        Iterator<Map.Entry<String, BranchModelsMeta>> it = userToBranchModelsInfoMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, BranchModelsMeta> entry = it.next();
            long resourceCreatedTime = entry.getValue().getResourceCreatedTime();
            if (resourceCreatedTime > 0 && currentTime - resourceCreatedTime > RESOURCE_OUTDATED_PERIOD) {
                log.info("deleteOutdatedResources(), delete outdated resource for {}", entry.getKey());
                it.remove();
            }
        }
    }

    @Override
    public void destroy() throws Exception {
        log.warn("Destroy method called. Cancelling requests for");

        // synchronized: Don't allow new requests creating new entries while destroying the plugin
        synchronized (userToBranchModelsInfoMap) {
            for (String userEmail : userToBranchModelsInfoMap.keySet()) {
                log.warn("  userEmail {}", userEmail);
                BranchModelsMeta branchModelsMeta = userToBranchModelsInfoMap.get(userEmail);
                branchModelsMeta.getFuture().cancel(true);
                // It seems to be this content is not destroyed, so remove it explicitly
                userToBranchModelsInfoMap.remove(userEmail);
            }
        }
        log.warn("  all threads canceled");
    }
}

