package jh.atlassian.stash.mybranches;

import com.atlassian.bitbucket.AuthorisationException;
import com.atlassian.bitbucket.permission.Permission;
import com.atlassian.bitbucket.permission.PermissionValidationService;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.StringReader;
import java.net.URI;
import java.util.*;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

// See stashbot plugin
// See https://developer.atlassian.com/docs/getting-started/learn-the-development-platform-by-example/store-and-retrieve-plugin-data
public class AdminConfigurationServlet extends HttpServlet {
    public static final String PLUGIN_SETTINGS_KEY = "jh.atlassian.stash.mybranches.adminconfig.";
    // The following ids are used 1) in SOY as HTML id, 2) as request parameter, 3) as plugin property parameter.
    public static final String MAX_COMMITS_ID = "mybranches_max_commits";
    public static final String MAX_COMMITS_ERROR_ID = "mybranches_max_commits_error";
    public static final String EXCLUDE_PROJECT_NAMES_REGEX_ID = "mybranches_exclude_project_names_regex";
    public static final String EXCLUDE_PROJECT_NAMES_REGEX_ERROR_ID = "mybranches_exclude_project_names_regex_error";
    public static final String EXCLUDE_PROJECT_KEYS_REGEX_ID = "mybranches_exclude_project_keys_regex";
    public static final String EXCLUDE_PROJECT_KEYS_REGEX_ERROR_ID = "mybranches_exclude_project_keys_regex_error";
    public static final String EXCLUDE_REPO_SLUGS_REGEX_ID = "mybranches_exclude_repo_slugs_regex";
    public static final String EXCLUDE_REPO_SLUGS_REGEX_ERROR_ID = "mybranches_exclude_repo_slugs_regex_error";
    public static final String EXCLUDE_BRANCHES_REGEX_ID = "mybranches_exclude_branches_regex";
    public static final String EXCLUDE_BRANCHES_REGEX_ERROR_ID = "mybranches_exclude_branches_regex_error";
    public static final String LIMIT_ADMIN_ROLE_ID = "mybranches_limit_admin_role_to";
    public static final String ALT_EMAIL_ADDRESSES_ID = "mybranches_alt_email";
    public static final String ALT_EMAIL_ADDRESSES_ERROR_ID = "mybranches_alt_email_error";
    public static final String IS_SETTINGS_SAVED = "mybranches_is_saved";
    private static final Logger log = LoggerFactory.getLogger(AdminConfigurationServlet.class);
    private static final String IS_SETTINGS_ERROR = "mybranches_is_error";
    private final AdminConfigService adminConfigService;
    private final SoyTemplateRenderer soyTemplateRenderer;
    private final PageBuilderService pageBuilderService;
    private final LoginUriProvider loginUriProvider;
    private final PermissionValidationService permissionValidationService;

    public AdminConfigurationServlet(SoyTemplateRenderer soyTemplateRenderer, PageBuilderService pageBuilderService, LoginUriProvider loginUriProvider,
                                     PermissionValidationService permissionValidationService, AdminConfigService adminConfigService) {
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.pageBuilderService = pageBuilderService;
        this.permissionValidationService = permissionValidationService;
        this.adminConfigService = adminConfigService;
        this.loginUriProvider = loginUriProvider;
    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

        // Authenticate user
        try {
            permissionValidationService.validateAuthenticated();
        } catch (AuthorisationException notLoggedInException) {
            // not logged in, redirect
            res.sendRedirect(loginUriProvider.getLoginUri(getUri(req)).toASCIIString());
            return;
        }
        try {
            permissionValidationService.validateForGlobal(Permission.SYS_ADMIN);
        } catch (AuthorisationException notAdminException) {
            res.sendError(HttpServletResponse.SC_UNAUTHORIZED, "You do not have permission to access this page.");
            return;
        }

        // Handle deletes
        // String pathInfo = req.getPathInfo();
        String relUrl = req.getRequestURL().toString();
        relUrl = relUrl.replaceAll("/+$", "").replaceAll("/delete/?.*$", "").replaceAll("/reload-all/?.*$", "").replaceAll("/create-new/?.*$", "").replaceAll
                ("\\?notice=.*$", "").replaceAll("\\?error=.*$", "");

        Properties properties;
        Map<String, Object> parametersMap = (Map<String, Object>) req.getAttribute(PLUGIN_SETTINGS_KEY);
        if (parametersMap == null) {
            // Direct call, not from doPost(). Get settings from admin configuration.
            AdminConfig adminConfig = adminConfigService.getAdminConfig();
            parametersMap = new HashMap<>();
            parametersMap.put(MAX_COMMITS_ID, adminConfig.getMaxCommits());
            parametersMap.put(EXCLUDE_PROJECT_NAMES_REGEX_ID, adminConfig.getExclProjNRegex());
            parametersMap.put(EXCLUDE_PROJECT_KEYS_REGEX_ID, adminConfig.getExclProjKRegex());
            parametersMap.put(EXCLUDE_REPO_SLUGS_REGEX_ID, adminConfig.getExclRepoSRegex());
            parametersMap.put(EXCLUDE_BRANCHES_REGEX_ID, adminConfig.getExclBranchRegex());
            parametersMap.put(LIMIT_ADMIN_ROLE_ID, adminConfig.getLimitAdminRole());
            parametersMap.put(ALT_EMAIL_ADDRESSES_ID, adminConfigService.getAlternativeEmailAddressesConfig());
            parametersMap.put(IS_SETTINGS_SAVED, false);
        }

        parametersMap.put("relUrl", relUrl);

        res.setContentType("text/html;charset=UTF-8");
        try {
            pageBuilderService.assembler().resources().requireContext("jh.atlassian.stash.mybranches");
            // group.artifact:resource-name
            soyTemplateRenderer.render(res.getWriter(), "jh.atlassian.stash.mybranches.stash-mybranches:mybranchesAdminConfigurationResources", "jh" +
                    ".atlassian" + ".stash.mybranches.adminConfigurationTemplate", parametersMap);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        try {
            permissionValidationService.validateForGlobal(Permission.SYS_ADMIN);
        } catch (AuthorisationException e) {
            doGet(req, res);
            return;
        }

        Map<String, Object> parametersMap = new HashMap<>();

        // Validate
        List<String> error;
        parametersMap.put(IS_SETTINGS_ERROR, false);

        parametersMap.put(MAX_COMMITS_ID, req.getParameter(MAX_COMMITS_ID));
        error = getCommitsCountError(req.getParameter(MAX_COMMITS_ID));
        if (!error.isEmpty()) {
            parametersMap.put(MAX_COMMITS_ERROR_ID, error);
            parametersMap.put(IS_SETTINGS_ERROR, true);
        }
        parametersMap.put(EXCLUDE_PROJECT_NAMES_REGEX_ID, req.getParameter(EXCLUDE_PROJECT_NAMES_REGEX_ID));
        error = getRegExError(req.getParameter(EXCLUDE_PROJECT_NAMES_REGEX_ID));
        if (!error.isEmpty()) {
            parametersMap.put(EXCLUDE_PROJECT_NAMES_REGEX_ERROR_ID, error);
            parametersMap.put(IS_SETTINGS_ERROR, true);
        }
        parametersMap.put(EXCLUDE_PROJECT_KEYS_REGEX_ID, req.getParameter(EXCLUDE_PROJECT_KEYS_REGEX_ID));
        error = getRegExError(req.getParameter(EXCLUDE_PROJECT_KEYS_REGEX_ID));
        if (!error.isEmpty()) {
            parametersMap.put(EXCLUDE_PROJECT_KEYS_REGEX_ERROR_ID, error);
            parametersMap.put(IS_SETTINGS_ERROR, true);
        }
        parametersMap.put(EXCLUDE_REPO_SLUGS_REGEX_ID, req.getParameter(EXCLUDE_REPO_SLUGS_REGEX_ID));
        error = getRegExError(req.getParameter(EXCLUDE_REPO_SLUGS_REGEX_ID));
        if (!error.isEmpty()) {
            parametersMap.put(EXCLUDE_REPO_SLUGS_REGEX_ERROR_ID, error);
            parametersMap.put(IS_SETTINGS_ERROR, true);
        }
        parametersMap.put(EXCLUDE_BRANCHES_REGEX_ID, req.getParameter(EXCLUDE_BRANCHES_REGEX_ID));
        error = getRegExError(req.getParameter(EXCLUDE_BRANCHES_REGEX_ID));
        if (!error.isEmpty()) {
            parametersMap.put(EXCLUDE_BRANCHES_REGEX_ERROR_ID, error);
            parametersMap.put(IS_SETTINGS_ERROR, true);
        }
        parametersMap.put(LIMIT_ADMIN_ROLE_ID, req.getParameter(LIMIT_ADMIN_ROLE_ID));
        parametersMap.put(ALT_EMAIL_ADDRESSES_ID, req.getParameter(ALT_EMAIL_ADDRESSES_ID));
        error = getAlternativeEmailAddressesError(req.getParameter(ALT_EMAIL_ADDRESSES_ID));
        if (!error.isEmpty()) {
            parametersMap.put(ALT_EMAIL_ADDRESSES_ERROR_ID, error);
            parametersMap.put(IS_SETTINGS_ERROR, true);
        }

        if (!(Boolean) parametersMap.get(IS_SETTINGS_ERROR)) {
            AdminConfig adminConfig = adminConfigService.getAdminConfig();
            String maxCommits = req.getParameter(MAX_COMMITS_ID);
            if (maxCommits == null || maxCommits.isEmpty()) {
                adminConfig.setMaxCommits(AdminConfigService.MAX_COMMITS_DEFAULT);
            } else {
                adminConfig.setMaxCommits(Integer.parseInt(req.getParameter(MAX_COMMITS_ID)));
            }
            adminConfig.setExclProjNRegex(req.getParameter(EXCLUDE_PROJECT_NAMES_REGEX_ID));
            adminConfig.setExclProjKRegex(req.getParameter(EXCLUDE_PROJECT_KEYS_REGEX_ID));
            adminConfig.setExclRepoSRegex(req.getParameter(EXCLUDE_REPO_SLUGS_REGEX_ID));
            adminConfig.setExclBranchRegex(req.getParameter(EXCLUDE_BRANCHES_REGEX_ID));
            adminConfig.setLimitAdminRole(req.getParameter(LIMIT_ADMIN_ROLE_ID));
            adminConfig.save();
            adminConfigService.setAlternativeEmailAddressesConfig(req.getParameter(ALT_EMAIL_ADDRESSES_ID));
            parametersMap.put(IS_SETTINGS_SAVED, true);
        }
        req.setAttribute(PLUGIN_SETTINGS_KEY, parametersMap);

        doGet(req, res); // reload after update
    }

    private URI getUri(HttpServletRequest request) {
        StringBuffer builder = request.getRequestURL();
        if (request.getQueryString() != null) {
            builder.append("?");
            builder.append(request.getQueryString());
        }
        return URI.create(builder.toString());
    }

    private List<String> getCommitsCountError(Object countObject) {
        if (countObject == null) {
            return Collections.emptyList();
        }
        String count = ((String) countObject).trim();
        if (count.isEmpty() || count.matches("[1-9]")) {
            return Collections.emptyList();
        }
        List<String> errors = new ArrayList<>();
        errors.add("Number must be in range from 1 to 9");
        return errors;
    }

    private List<String> getRegExError(Object regexObject) {
        if (regexObject == null) {
            return Collections.emptyList();
        }
        try {
            Pattern.compile((String) regexObject);
            return Collections.emptyList();
        } catch (PatternSyntaxException e) {
            // AUI form expects an list of errors, even if there is only one
            List<String> errors = new ArrayList<>();
            errors.add("Not a valid regular expression:<br/><pre>" + e.getLocalizedMessage() + "</pre>");
            return errors;
        }
    }

    private List<String> getAlternativeEmailAddressesError(String alternativeEmailAddresses) {
        try {
            Properties properties = new Properties();
            properties.load(new StringReader(alternativeEmailAddresses));
            return Collections.emptyList();
        } catch (IOException | IllegalArgumentException e) {
            return Arrays.asList(e.getMessage());
        }
    }
}
