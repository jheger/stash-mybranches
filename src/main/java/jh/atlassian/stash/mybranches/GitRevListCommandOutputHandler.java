package jh.atlassian.stash.mybranches;

import com.atlassian.bitbucket.io.BaseCommandHandler;
import com.atlassian.bitbucket.io.StringOutputHandler;
import com.atlassian.bitbucket.scm.CommandOutputHandler;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class GitRevListCommandOutputHandler extends BaseCommandHandler implements CommandOutputHandler<List<BranchModel.CommitModel>> {
    private static final String LINE_DELIMITER_REGEX = "[\\n\\r]+";
    private static final Logger log = LoggerFactory.getLogger(GitRevListCommand.class);
    private List<BranchModel.CommitModel> commitModels = new ArrayList<>();

    @Override
    public List<BranchModel.CommitModel> getOutput() {
        return commitModels;
    }

    @Override
    public void process(InputStream inputStream)  {
        String output = null;
        try {
            output = IOUtils.toString(inputStream, "UTF-8");
            log.debug("Output of git rev-list:\n{}", output);
            String[] lines = output.split(LINE_DELIMITER_REGEX);
            commitModels = parseCommits(lines);
        } catch (IOException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
    }

    private List<BranchModel.CommitModel> parseCommits(String[] lines) {
        List<BranchModel.CommitModel> commitModels = new ArrayList<>();
        for (int i = 0; i < lines.length; i++) {
            // Get the id and the number of parents. The parents are calculated by counting the spaces.
            // E. g. one parent:
            // commit 1000000000000000000000000000000000000000 2000000000000000000000000000000000000000
            // E. g. two parents:
            // commit 1000000000000000000000000000000000000000 2000000000000000000000000000000000000000 3000000000000000000000000000000000000000
            String id = lines[i].substring(7, 47);
            int parentCount = StringUtils.countMatches(lines[i], " ") - 1;
            String displayId = lines[++i].trim();
            String name = lines[++i].trim();
            String mailAddress = lines[++i].trim();
            String subject = lines[++i].trim();
            commitModels.add(new BranchModel.CommitModel(id, displayId, name, mailAddress, subject, parentCount));
        }
        return commitModels;
    }
}
