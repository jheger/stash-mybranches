package jh.atlassian.stash.mybranches;

import com.atlassian.activeobjects.external.ActiveObjects;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.StringReader;
import java.util.*;

import static com.google.common.base.Preconditions.checkNotNull;

public class AdminConfigServiceImpl implements AdminConfigService {
    private static final Logger log = LoggerFactory.getLogger(AdminConfigServiceImpl.class);
    private final ActiveObjects activeObjects;
    private AdminConfig cachedAdminConfig = null;

    public AdminConfigServiceImpl(ActiveObjects activeObjects) {
        this.activeObjects = checkNotNull(activeObjects);
    }

    public static String adminConfigsToString(AdminConfig[] adminConfigs) {
        return adminConfigsToString(Arrays.asList(adminConfigs));
    }

    static public StringBuilder adminConfigToString(AdminConfig adminConfig) {
        StringBuilder sb = new StringBuilder("[");
        sb.append("maxCommits ").append(adminConfig.getMaxCommits());
        sb.append(", exclProjNRegex ").append(adminConfig.getExclProjNRegex());
        sb.append(", exclProjKRegex ").append(adminConfig.getExclProjKRegex());
        sb.append(", exclRepoSRegex ").append(adminConfig.getExclRepoSRegex());
        sb.append(", exclBranchRegex ").append(adminConfig.getExclBranchRegex());
        sb.append(", limitAdminRoleToFind ").append(adminConfig.getLimitAdminRole());
        sb.append(", altEmailAddr ").append(adminConfig.getAltEmail());
        return sb.append("]");
    }

    public static String adminConfigsToString(List<AdminConfig> adminConfigs) {
        StringBuilder sb = new StringBuilder();
        for (AdminConfig adminConfig : adminConfigs) {
            sb.append(adminConfigToString(adminConfig)).append(", ");
        }
        return sb.toString();
    }

    private AdminConfig createInitialAdminConfig() {
        AdminConfig adminConfig = activeObjects.create(AdminConfig.class);
        adminConfig.setMaxCommits(AdminConfigService.MAX_COMMITS_DEFAULT);
        adminConfig.save();
        return adminConfig;
    }

    @Override
    public synchronized
    @Nonnull
    AdminConfig getAdminConfig() {
        AdminConfig adminConfig;
        if (cachedAdminConfig == null) {
            AdminConfig[] adminConfigs = activeObjects.find(AdminConfig.class);
            if (adminConfigs.length == 0) {
                adminConfig = createInitialAdminConfig();
            } else if (adminConfigs.length > 1) {
                log.error("Found more than 1 admin configuration. Deleting all of them and creating 1 new one. The configs were: {}",
                        adminConfigsToString(adminConfigs));
                activeObjects.delete(adminConfigs);
                adminConfig = createInitialAdminConfig();
            } else {
                adminConfig = adminConfigs[0];
            }
            cachedAdminConfig = adminConfig;
        }
        return cachedAdminConfig;
    }

    @Override
    public
    @Nonnull
    String getAlternativeEmailAddressesConfig() {
        return getAdminConfig().getAltEmail();
    }

    @Override
    public void setAlternativeEmailAddressesConfig(String alternativeEmailAddressesConfig) {
        AdminConfig adminConfig = getAdminConfig();
        adminConfig.setAltEmail(alternativeEmailAddressesConfig);
        adminConfig.save();
    }

    @Override
    public
    @Nonnull
    List<String> getCurrentAndAlternativeEmailAdresses(String forEmailAddress) {
        List<String> currentAndAlternativeEmailAddresses = new ArrayList<>();
        currentAndAlternativeEmailAddresses.add(forEmailAddress);
        String alternativeEmailAddressesConfig = getAdminConfig().getAltEmail();
        if (alternativeEmailAddressesConfig == null || alternativeEmailAddressesConfig.isEmpty()) {
            return currentAndAlternativeEmailAddresses;
        }
        try {
            Properties properties = new Properties();
            properties.load(new StringReader(getAdminConfig().getAltEmail()));
            String altEmailAddresses = properties.getProperty(forEmailAddress);
            if (altEmailAddresses != null) {
                Collections.addAll(currentAndAlternativeEmailAddresses, altEmailAddresses.trim().split("[\\s,;]+"));
            }
        } catch (IOException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
        return currentAndAlternativeEmailAddresses;
    }
}
