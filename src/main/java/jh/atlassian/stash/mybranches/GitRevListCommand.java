package jh.atlassian.stash.mybranches;

import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.scm.git.command.GitCommandBuilderFactory;
import jh.atlassian.stash.mybranches.BranchModel.CommitModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

// https://answers.atlassian.com/questions/11974547/git-shortlog
public class GitRevListCommand {
    private static final Logger log = LoggerFactory.getLogger(GitRevListCommand.class);
    private final GitCommandBuilderFactory gitCommandBuilderFactory;
    private final int maxCommits;

    public GitRevListCommand(GitCommandBuilderFactory gitCommandBuilderFactory, int maxCommits) {
        this.maxCommits = maxCommits;
        this.gitCommandBuilderFactory = gitCommandBuilderFactory;
    }

    public Map<String, List<CommitModel>> call(Repository repository, List<BranchModel> pagedBranchModels) {
        Map<String, List<BranchModel.CommitModel>> branchToCommitModelsMap = new HashMap<>();
        for (BranchModel branchModel : pagedBranchModels) {
            GitRevListCommandOutputHandler gitRevListOutputHandler = new GitRevListCommandOutputHandler();
            String branchName = branchModel.getBranch().getId();
            log.debug("Calling git rev-list on branch {}...", branchName);
            gitCommandBuilderFactory.builder(repository)
                    .argument("rev-list")
                    .argument("--first-parent")
                    .argument("--parents")
                    .argument("-n " + maxCommits)
                    .argument("--format=format: %h,%ad%n %an%n %ae%n %s")
                    .argument("--date=local")
                    .argument(branchName).build(gitRevListOutputHandler).call();
            branchToCommitModelsMap.put(branchName, gitRevListOutputHandler.getOutput());
        }
        return branchToCommitModelsMap;
    }

}
