package jh.atlassian.stash.mybranches;

import net.java.ao.Entity;
import net.java.ao.Preload;
import net.java.ao.schema.StringLength;
import net.java.ao.schema.Table;

@Preload
@Table("adminconfig")
public interface AdminConfig extends Entity {
    int getMaxCommits();

    void setMaxCommits(int maxCommits);

    // Project names
    String getExclProjNRegex();

    // Project names
    void setExclProjNRegex(String regex);

    // Project keys
    String getExclProjKRegex();

    // Project keys
    void setExclProjKRegex(String regex);

    // Repo slugs
    String getExclRepoSRegex();

    // Repo slugs
    void setExclRepoSRegex(String regex);

    String getExclBranchRegex();

    void setExclBranchRegex(String regex);

    String getLimitAdminRole();

    void setLimitAdminRole(String visibility);

    @StringLength(StringLength.UNLIMITED)
    String getAltEmail();

    void setAltEmail(String alternativeEmailAddresses);
}
