package jh.atlassian.stash.mybranches;

import com.atlassian.bitbucket.avatar.AvatarRequest;
import com.atlassian.bitbucket.avatar.AvatarService;
import com.atlassian.bitbucket.permission.Permission;
import com.atlassian.bitbucket.permission.PermissionService;
import com.atlassian.bitbucket.pull.PullRequest;
import com.atlassian.bitbucket.pull.PullRequestOrder;
import com.atlassian.bitbucket.pull.PullRequestSearchRequest;
import com.atlassian.bitbucket.pull.PullRequestService;
import com.atlassian.bitbucket.repository.*;
import com.atlassian.bitbucket.repository.ref.restriction.RefAccessRequest;
import com.atlassian.bitbucket.repository.ref.restriction.RefAccessType;
import com.atlassian.bitbucket.repository.ref.restriction.RefRestrictionService;
import com.atlassian.bitbucket.scm.git.command.GitCommandBuilderFactory;
import com.atlassian.bitbucket.server.ApplicationPropertiesService;
import com.atlassian.bitbucket.user.SecurityService;
import com.atlassian.bitbucket.util.Operation;
import com.atlassian.bitbucket.util.Page;
import com.atlassian.bitbucket.util.PageRequest;
import com.atlassian.bitbucket.util.PageRequestImpl;
import jh.atlassian.stash.mybranches.BranchModel.CommitModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

public class MyBranchesCollector implements Callable<List<BranchModel>> {
    private static final Logger log = LoggerFactory.getLogger(MyBranchesCollector.class);
    private static final int BRANCH_PAGE_SIZE = 500;
    private final ApplicationPropertiesService applicationPropertiesService;
    private final AvatarService avatarService;
    private final PermissionService permissionService;
    private final PullRequestService pullRequestService;
    private final RefRestrictionService refRestrictionService;
    private final RefService refService;
    private final SecurityService securityService;
    private final String excludeBranchesRegex;
    private final List<BranchModel> branchModels = new ArrayList<>();
    private final BranchModelsMeta branchModelsMeta;
    private final List<String> currentAndAlternativeEmailAdresses;
    private final GitCommandBuilderFactory gitCommandBuilderFactory;

    MyBranchesCollector(ApplicationPropertiesService applicationPropertiesService, AvatarService avatarService, PermissionService permissionService,
                        PullRequestService pullRequestService, RefRestrictionService refRestrictionService, RefService refService, SecurityService
                                securityService, GitCommandBuilderFactory gitCommandBuilderFactory,
                        BranchModelsMeta branchModelsMeta, String excludeBranchesRegex,
                        List<String> currentAndAlternativeEmailAdresses) {
        this.applicationPropertiesService = applicationPropertiesService;
        this.avatarService = avatarService;
        this.permissionService = permissionService;
        this.pullRequestService = pullRequestService;
        this.refRestrictionService = refRestrictionService;
        this.refService = refService;
        this.securityService = securityService;
        this.gitCommandBuilderFactory = gitCommandBuilderFactory;
        this.excludeBranchesRegex = excludeBranchesRegex;
        this.branchModelsMeta = branchModelsMeta;
        this.currentAndAlternativeEmailAdresses = currentAndAlternativeEmailAdresses;
    }

    @Override
    public List<BranchModel> call() throws Exception {
        // repositoryMetadataService.getBranches() fails with
        // java.util.concurrent.ExecutionException: org.springframework.security.authentication.AuthenticationCredentialsNotFoundException: An Authentication
        // object was not found in the SecurityContext
        // if not encapsulated in explicit user context. Therefore here impersonating().
        securityService.impersonating(branchModelsMeta.getApplicationUser(), "Add-on My Branches collects commit infos")
                .call((Operation<List<BranchModel>, RuntimeException>) () -> {
                    collectBranches();
                    return null;
                });

        return branchModels;
    }

    private void collectBranches() {
        log.debug("collectBranches(), user '{}', repos count {}, currentAndAlternativeEmailAdresses {}",
                branchModelsMeta.getEmailAddress(), branchModelsMeta.getRepositories().size(), currentAndAlternativeEmailAdresses);
        for (Repository repository : branchModelsMeta.getRepositories()) {
            boolean secure = true; // ?
            int size = 24;
            boolean useConfigured = false; // Let URL start with /stash instead of http
            String projectAvatarUrl = avatarService.getUrlForProject(repository.getProject(), new AvatarRequest(secure, size, useConfigured));
            GitRevListCommand gitRevListCommand = new GitRevListCommand(gitCommandBuilderFactory, branchModelsMeta.getMaxCommits());
            boolean isRepoWritePermission = permissionService.hasRepositoryPermission(repository, Permission.REPO_WRITE);
            log.debug("  collectBranches(), user '{}', repository '{}', isRepoWritePermission {}",
                    branchModelsMeta.getEmailAddress(), repository.getSlug(), isRepoWritePermission);
            RepositoryBranchesRequest.Builder builder = new RepositoryBranchesRequest.Builder(repository).order(RefOrder.ALPHABETICAL);
            PageRequest branchPageRequest = new PageRequestImpl(0, BRANCH_PAGE_SIZE);
            while (branchPageRequest != null) {
                Page<Branch> pagedBranches = refService.getBranches(builder.build(), branchPageRequest);
                log.debug("    collectBranches(), user '{}', repository '{}', BRANCH_PAGE_SIZE {}",
                        branchModelsMeta.getEmailAddress(), repository.getSlug(), BRANCH_PAGE_SIZE);
                List<BranchModel> pagedBranchModels = new ArrayList<>();
                for (Branch branch : pagedBranches.getValues()) {
                    if (excludeBranchesRegex != null && branch.getDisplayId().matches(excludeBranchesRegex)) {
                        log.debug("    collectBranches(), user '{}', repository '{}', branch '{}': skip branch because of excludeBranchesRegex",
                                branchModelsMeta.getEmailAddress(), repository.getSlug(), branch.getDisplayId());
                        continue;
                    }
                    log.debug("    collectBranches(), user '{}', repository '{}': schedule branch for analyzing '{}'",
                            branchModelsMeta.getEmailAddress(), repository.getSlug(), branch.getDisplayId());
                    pagedBranchModels.add(new BranchModel(branch, repository));
                }
                log.debug("    collectBranches(), user '{}', repository '{}': checking commits of {} branches",
                        branchModelsMeta.getEmailAddress(), repository.getSlug(), pagedBranchModels.size());

                if (!pagedBranchModels.isEmpty()) {
                    Map<String, List<CommitModel>> branchToCommitModelsMap = gitRevListCommand.call(repository, pagedBranchModels);
                    // If an error occurs in gitRevListCommand.call(), the branchToCommitModelsMap is empty. None of the branches on this
                    // branch page is present. Skip this page.
                    if (branchToCommitModelsMap.isEmpty()) {
                        log.error("collectBranches(), user '{}', repository '{}': branchToCommitModelsMap is empty. Skipping all branches on this branch page.",
                                branchModelsMeta.getEmailAddress(), repository.getSlug());
                        branchPageRequest = pagedBranches.getNextPageRequest();
                        continue;
                    }
                    log.debug("    branchToCommitModelsMap size is {}", branchToCommitModelsMap.size());
                    for (BranchModel pagedBranchModel : pagedBranchModels) {
                        List<CommitModel> commitModels = branchToCommitModelsMap.get(pagedBranchModel.getBranch().getId());
                        log.debug("    collectBranches(), user '{}', repository '{}': checking commits of branch '{}'",
                                branchModelsMeta.getEmailAddress(), repository.getSlug(), pagedBranchModel.getBranch().getDisplayId());
                        // At least one commit with current user's email address or alternative email addresses?
                        boolean isContainsOwnCommit = false;
                        Set<String> committers = new HashSet<>();
                        for (CommitModel commitModel : commitModels) {
                            committers.add(commitModel.getAuthorEmailAddress());
                            if (currentAndAlternativeEmailAdresses.contains(commitModel.getAuthorEmailAddress())) {
                                commitModel.setOwnCommit(true);
                                isContainsOwnCommit = true;
                            }
                        }
                        log.debug("    collectBranches(), user '{}', repository '{}', branch '{}': committers {}",
                                branchModelsMeta.getEmailAddress(), repository.getSlug(), pagedBranchModel.getBranch().getDisplayId(), committers);
                        if (!isContainsOwnCommit) {
                            log.debug("    collectBranches(), user '{}', repository '{}': branch '{}' contains no commits of current user, skip it",
                                    branchModelsMeta.getEmailAddress(), repository.getSlug(), pagedBranchModel.getBranch().getDisplayId());
                            continue;
                        }
                        log.debug("    collectBranches(), user '{}', repository '{}': branch '{}' contains commits of current user, keep it",
                                branchModelsMeta.getEmailAddress(), repository.getSlug(), pagedBranchModel.getBranch().getDisplayId());
                        pagedBranchModel.setCommitModels(commitModels);
                        pagedBranchModel.setProjectAvatarUrl(projectAvatarUrl);
                        if (isRepoWritePermission) {
                            RefAccessRequest refAccessRequest = new RefAccessRequest.Builder(repository, RefAccessType.DELETE).ref(pagedBranchModel.getBranch
                                    ()).build();
                            boolean isAllowDelete = refRestrictionService.hasPermission(refAccessRequest);
                            pagedBranchModel.setAllowDelete(isAllowDelete);
                            log.debug("    collectBranches(), user '{}', repository '{}', branch '{}': isAllowDelete {}", branchModelsMeta.getEmailAddress(),
                                    repository.getSlug(), pagedBranchModel.getBranch().getDisplayId(), isAllowDelete);
                        }
                        List<PullRequest> pullRequests = searchPullRequestsForBranch(pagedBranchModel.getBranch().getId(), repository);
                        pagedBranchModel.setPullRequests(pullRequests);
                        for (PullRequest pullRequest : pullRequests) {
                            if (pullRequest.isOpen()) {
                                pagedBranchModel.setAllowDelete(false);
                                break;
                            }
                        }
                        branchModels.add(pagedBranchModel);
                    }
                }
                branchPageRequest = pagedBranches.getNextPageRequest();
            }
            branchModelsMeta.incRepositoryProgress();
        }

        log.info("collectBranches(), user '{}': include branches {}", branchModelsMeta.getEmailAddress(),
                branchModels.stream().map(branchModel -> branchModel.getBranch().getDisplayId()).collect(Collectors.toList()));

        log.debug("collectBranches(), user '{}', finished", branchModelsMeta.getEmailAddress());
    }

    private List<PullRequest> searchPullRequestsForBranch(String branchName, Repository repository) {
        PageRequest pageRequest = new PageRequestImpl(0, 100);
        PullRequestSearchRequest searchRequest = new PullRequestSearchRequest.Builder().toRepositoryId(repository.getId()).fromRefId(branchName).order
                (PullRequestOrder.NEWEST).build();
        Page<PullRequest> pagedPullRequests = pullRequestService.search(searchRequest, pageRequest);
        List<PullRequest> pullRequests = new ArrayList<>();
        for (PullRequest pullRequest : pagedPullRequests.getValues()) {
            pullRequests.add(pullRequest);
        }
        return pullRequests;
    }
}
