package ut.jh.atlassian.stash.mybranches;

import jh.atlassian.stash.mybranches.BranchModel.CommitModel;
import jh.atlassian.stash.mybranches.GitRevListCommandOutputHandler;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNull.notNullValue;

// All outputs with command: git show <tag name> -q
public class GitRevListCommandOutputHandlerTest {

    @Test
    public void process1BranchWith1CommitTest() throws IOException {

        String s = "commit 1000000000000000000000000000000000000000 2000000000000000000000000000000000000000\r\n"
                + " 10000000\r\n"
                + " Lea\r\n"
                + " lea@example.com\r\n"
                + " commit message 1\r\n";

        GitRevListCommandOutputHandler outputHandler = new GitRevListCommandOutputHandler();
        outputHandler.process(IOUtils.toInputStream(s, "UTF-8"));
        List<CommitModel> commitModels = outputHandler.getOutput();
        assertThat(commitModels, is(notNullValue()));
        assertThat(commitModels.size(), is(1));
        assertThat(commitModels.get(0).getId(), is("1000000000000000000000000000000000000000"));
        assertThat(commitModels.get(0).getDisplayId(), is("10000000"));
        assertThat(commitModels.get(0).getAuthorName(), is("Lea"));
        assertThat(commitModels.get(0).getAuthorEmailAddress(), is("lea@example.com"));
        assertThat(commitModels.get(0).getSubject(), is("commit message 1"));
        assertThat(commitModels.get(0).getParentCount(), is(1));
    }

    @Test
    public void process1BranchWith3CommitTest() throws IOException {

        String s = "commit 1000000000000000000000000000000000000000 2000000000000000000000000000000000000000\r\n"
                + " 10000000\r\n"
                + " Alex\r\n"
                + " alex@example.com\r\n"
                + " commit message 1\r\n"
                + "commit 2000000000000000000000000000000000000000 3000000000000000000000000000000000000000\r\n"
                + " 20000000\r\n"
                + " Lea\r\n"
                + " lea@example.com\r\n"
                + " commit message 2\r\n"
                + "commit 3000000000000000000000000000000000000000 4000000000000000000000000000000000000000\r\n"
                + " 30000000\r\n"
                + " Alex\r\n"
                + " alex@example.com\r\n"
                + " commit message 3\r\n";

        GitRevListCommandOutputHandler outputHandler = new GitRevListCommandOutputHandler();
        outputHandler.process(IOUtils.toInputStream(s, "UTF-8"));
        List<CommitModel> commitModels = outputHandler.getOutput();

        assertThat(commitModels.size(), is(3));

        assertThat(commitModels.get(0).getId(), is("1000000000000000000000000000000000000000"));
        assertThat(commitModels.get(0).getDisplayId(), is("10000000"));
        assertThat(commitModels.get(0).getAuthorName(), is("Alex"));
        assertThat(commitModels.get(0).getAuthorEmailAddress(), is("alex@example.com"));
        assertThat(commitModels.get(0).getSubject(), is("commit message 1"));
        assertThat(commitModels.get(0).getParentCount(), is(1));

        assertThat(commitModels.get(1).getId(), is("2000000000000000000000000000000000000000"));
        assertThat(commitModels.get(1).getDisplayId(), is("20000000"));
        assertThat(commitModels.get(1).getAuthorName(), is("Lea"));
        assertThat(commitModels.get(1).getAuthorEmailAddress(), is("lea@example.com"));
        assertThat(commitModels.get(1).getSubject(), is("commit message 2"));
        assertThat(commitModels.get(1).getParentCount(), is(1));

        assertThat(commitModels.get(2).getId(), is("3000000000000000000000000000000000000000"));
        assertThat(commitModels.get(2).getDisplayId(), is("30000000"));
        assertThat(commitModels.get(2).getAuthorName(), is("Alex"));
        assertThat(commitModels.get(2).getAuthorEmailAddress(), is("alex@example.com"));
        assertThat(commitModels.get(2).getSubject(), is("commit message 3"));
        assertThat(commitModels.get(2).getParentCount(), is(1));
    }

    @Test
    public void processSpecialCharsTest() throws IOException {

        String s = "commit 1000000000000000000000000000000000000000 2000000000000000000000000000000000000000\r\n"
                + " 10000000\r\n"
                + " Lea\r\n"
                + " lea@example.com\r\n"
                + " Łopaciński\r\n";

        GitRevListCommandOutputHandler outputHandler = new GitRevListCommandOutputHandler();
        outputHandler.process(IOUtils.toInputStream(s, "UTF-8"));
        List<CommitModel> commitModels = outputHandler.getOutput();

        assertThat(commitModels.size(), is(1));
        assertThat(commitModels.get(0).getId(), is("1000000000000000000000000000000000000000"));
        assertThat(commitModels.get(0).getDisplayId(), is("10000000"));
        assertThat(commitModels.get(0).getAuthorName(), is("Lea"));
        assertThat(commitModels.get(0).getAuthorEmailAddress(), is("lea@example.com"));
        assertThat(commitModels.get(0).getSubject(), is("Łopaciński"));
        assertThat(commitModels.get(0).getParentCount(), is(1));
    }
}
