#!/bin/bash


#set -x


# See "Duplicating a repository"
# https://help.github.com/articles/duplicating-a-repository/
#
#   git clone --bare https://github.com/exampleuser/old-repository.git
#   cd old-repository.git
#   git push --mirror https://github.com/exampleuser/new-repository.git



#-----------
# Globals
#-----------
g_url_scheme='http'
g_bitbucket_url='localhost:7990/bitbucket'
g_user='admin'
g_password='admin'



#-----------
# Functions
#-----------


# Convenience function for curl
function curl_post {
    local url="$1"
    local jsonData="$2"
    
    curl -H 'Content-type: application/json' -u 'admin:admin' --data "$jsonData" "$g_url_scheme://$g_bitbucket_url/$url" >/dev/null
}


# Convenience function for curl
function curl_post_by_user {
    local url="$1"
    local jsonData="$2"
    local user="$3"
    
    curl -H 'Content-type: application/json' -u "$user:$user" --data "$jsonData" "$g_url_scheme://$g_bitbucket_url/$url" >/dev/null
}


# Create a new user.
function create_user {
    local name="$1"
    local displayName=`echo $2 | sed 's/ /%20/g'`
    local emailAddress="$3"
    
    local data="{}"
    curl_post "rest/api/1.0/admin/users?name=$name&password=$name&displayName=$displayName&emailAddress=$emailAddress&notify=false" "$data"
}


# Add a user to one or more groups.
function add_group {
    local user="$1"
    local group="$2"
    
    local data="{\"user\":\"$user\",\"groups\":[\"$group\"]}"
    curl_post 'rest/api/1.0/admin/users/add-groups' "$data"
}


# Promote or demote the global permission level of a user. Available global permissions are:
#   o LICENSED_USER
#   o PROJECT_CREATE
#   o ADMIN
#   o SYS_ADMIN
function add_permission {
    local name="$1"
    local permission="$2"
    
    curl -u 'admin:admin' -X PUT "$g_url_scheme://$g_bitbucket_url/rest/api/1.0/admin/permissions/users?name=$name&permission=$permission" >/dev/null
}
 

# Create a new project.
function create_project {
    local key="$1"
    local name="$2"
    local description="$3"
    
    local data="{\"key\": \"$key\", \"name\": \"$name\", \"description\": \"$description\"}"
    curl_post 'rest/api/1.0/projects' "$data"
}


function delete_project {
    local projectKey="$1"
    
    curl -u 'admin:admin' -X DELETE "$g_url_scheme://$g_bitbucket_url/rest/api/1.0/projects/$projectKey" >/dev/null
}


# Create a new repository.
function create_repository {
    local projectKey="$1"
    local name="$2"
    
    local data="{\"name\": \"$name\", \"scmId\": \"git\"}"    
    curl_post "rest/api/1.0/projects/$projectKey/repos" "$data"
}


# Create a new repository forked from an existing repository. 
function fork_repository {
    local fromProjectKey="$1"
    local fromRepositoryName="$2"
    local toProjectKey="$3"
    local toRepositoryName="$4"

    local data="{\"slug\": \"$toRepositoryName\",\"name\":\"$toRepositoryName\",\"project\":{\"key\":\"$toProjectKey\"}}"
    curl_post "rest/api/1.0/projects/$fromProjectKey/repos/$fromRepositoryName" "$data"
}


# Create a new repository forked from an existing repository. 
function fork_personal_repository {
    local fromProjectKey="$1"
    local fromRepositoryName="$2"
    local userName="$3"
    local toRepositoryName="$4"

    local data="{\"slug\": \"$toRepositoryName\",\"name\":\"$toRepositoryName\",\"project\":{\"key\":\"~$userName\"}}"
    curl_post_by_user "rest/api/1.0/projects/$fromProjectKey/repos/$fromRepositoryName" "$data" "$userName"
}


function delete_repository {
    local projectKey="$1"
    local repositorySlug="$2"
    
    curl -u 'admin:admin' -X DELETE "$g_url_scheme://$g_bitbucket_url/rest/api/1.0/projects/$projectKey/repos/$repositorySlug" >/dev/null
}


function set_project_permission {
    local projectKey="$1"
    local permission="$2"
    
    curl_post "rest/api/1.0/projects/$projectKey/permissions/$permission/all?allow=true"
}


# Mirror the bare clone
function push_repository {
    local projectKey="$1"
    local localRepositoryScmId="$2"
    local remoteRepositoryScmId="$3"
    
    cd "$localRepositoryScmId"
    git push --mirror "$g_url_scheme://$g_user:$g_password@$g_bitbucket_url/scm/$projectKey/$remoteRepositoryScmId" -q
    cd ..
}


#-----------
# Main
#-----------

# Delete defaults
delete_repository 'PROJECT_1' 'rep_1'
delete_project 'PROJECT_1'

# Create users
create_user 'alek' 'Alek Mierzwicki' 'amierzwicki@atlassian.com'
create_user 'dariusz' 'Dariusz Szuksztul (Admin)' 'dszuksztul@atlassian.com'
add_permission 'dariusz' 'SYS_ADMIN'
create_user 'marek' 'Marek Pazik' 'mpazik@atlassian.com'
create_user 'other' 'Other User' 'other@example.com'

# Create projects and repos
repositoryTemplateName='jira-rest-java-client.git'

projectKey='P0'
repositoryName='This-is-a-repository-with-a-long-name'
create_project "$projectKey" 'Project-with-long-project-name' 'Description'
set_project_permission "$projectKey" 'PROJECT_WRITE'
create_repository "$projectKey" "$repositoryName"
push_repository "$projectKey" "$repositoryTemplateName" "$repositoryName"


projectKey='PWSIN'
repositoryName='Repo With Spaces In Name'
repositorySlug='repo-with-spaces-in-name'
create_project "$projectKey" 'Project With Spaces In Name' 'Description with spaces'
set_project_permission "$projectKey" 'PROJECT_WRITE'
create_repository "$projectKey" "$repositoryName"
push_repository "$projectKey" "$repositoryTemplateName" "$repositorySlug"


noOfProjects=2
noOfRepositories=2

project1Key=""
repository1Name=""

for projectNo in `eval echo {1..$noOfProjects}`
do
    projectKey="P$projectNo"
    echo "create project $projectKey..."
    create_project "$projectKey" "Project-$projectKey" "Description of Project-$projectKey"

    set_project_permission "$projectKey" 'PROJECT_WRITE'

    for repositoryNo in `eval echo {1..$noOfRepositories}`
    do
        repositoryName="repo-$projectKey-r$repositoryNo"

        # Push only one repository and fork the others. This is much more faster than pushing them all.
        if (( $projectNo == 1 && $repositoryNo == 1 ))
        then
            project1Key="$projectKey"
            repository1Name="$repositoryName"
            echo "create repository $repositoryName..."
            create_repository "$projectKey" "$repositoryName"
            echo "push repository $repositoryName..."
            push_repository "$projectKey" "$repositoryTemplateName" "$repositoryName"
        else
            echo "fork repository $project1Key/$repositoryName to $projectKey/$repositoryName..."
            fork_repository "$project1Key" "$repository1Name" "$projectKey" "$repositoryName"
        fi
    done
done

fork_personal_repository 'P1' 'repo-P1-r1' 'alek' 'alek-personal-repo1'


#------------------------------------------------------------------------------
# Create test repo for branches with 1 to 10 commits
#------------------------------------------------------------------------------

repo_name='1-10-commits-repo'
rm -rf "$repo_name"
mkdir "$repo_name"
cd "$repo_name"

author_1='admin <admin@example.com>'
author_2='user <user@example.com>'
git init
# Commit to master
git commit --allow-empty -m 'commit 1' --author="$author_1"
# Commit to branches
git checkout -b 2commits
git commit --allow-empty -m 'commit 2' --author="$author_2"
git checkout -b 3commits
git commit --allow-empty -m 'commit 3' --author="$author_1"
git checkout -b 4commits
git commit --allow-empty -m 'commit 4' --author="$author_2"
git checkout -b 5commits
git commit --allow-empty -m 'commit 5' --author="$author_1"
git checkout -b 6commits
git commit --allow-empty -m 'commit 6' --author="$author_2"
git checkout -b 7commits
git commit --allow-empty -m 'commit 7' --author="$author_1"
git checkout -b 8commits
git commit --allow-empty -m 'commit 8' --author="$author_2"
git checkout -b 9commits
git commit --allow-empty -m 'commit 9' --author="$author_1"
git checkout -b 10commits
git commit --allow-empty -m 'commit 10' --author="$author_2"

project_name='P1-10Commits'
create_project "$project_name" "$project_name" "$project_name"
set_project_permission "$project_name" 'PROJECT_WRITE'
create_repository "$project_name" "$repo_name"
push_repository "$project_name" '.' "$repo_name"


#------------------------------------------------------------------------------
# Create test repo for umlaut and HTML escape
#------------------------------------------------------------------------------

repo_name='umlaut-and-escape-repo'
rm -rf "$repo_name"
mkdir "$repo_name"
cd "$repo_name"

author_1='admin <admin@example.com>'
git init
git commit --allow-empty -m 'Ä' --author="$author_1"
git commit --allow-empty -m '<strike>commit</strike>' --author="$author_1"

project_name='P_UmlautAndEscape'
create_project "$project_name" "$project_name" "$project_name"
set_project_permission "$project_name" 'PROJECT_WRITE'
create_repository "$project_name" "$repo_name"
push_repository "$project_name" '.' "$repo_name"


#------------------------------------------------------------------------------
# Create test repo for alternative email addresses
#------------------------------------------------------------------------------

repo_name='alternative-email-addresses'
rm -rf "$repo_name"
mkdir "$repo_name"
cd "$repo_name"

other='other <other@example.com>'
user='user <user@example.com>'
user_legacy='user legacy<user_legacy@example.com>'
user_legacy2='user legacy2<user_legacy2@example.com>'
git init
# Commit to master
git commit --allow-empty -m 'master 1' --author="$other"
# Commit to branches
git checkout -b other-commits
git checkout -b user-commits
git checkout -b user-legacy-commits
git checkout -b user-legacy2-commits
git checkout -b user-and-user-legacy-commits

git checkout other-commits
git commit --allow-empty -m 'others-commits 1' --author="$other"
git commit --allow-empty -m 'others-commits 2' --author="$other"
git commit --allow-empty -m 'others-commits 3' --author="$other"
git commit --allow-empty -m 'others-commits 4' --author="$other"
git commit --allow-empty -m 'others-commits 5' --author="$other"

git checkout user-commits
git commit --allow-empty -m 'user-commits 1' --author="$other"
git commit --allow-empty -m 'user-commits 2' --author="$other"
git commit --allow-empty -m 'user-commits 3' --author="$other"
git commit --allow-empty -m 'user-commits 4' --author="$user"            # !
git commit --allow-empty -m 'user-commits 5' --author="$other"

git checkout user-legacy-commits
git commit --allow-empty -m 'user-legacy-commits 1' --author="$other"
git commit --allow-empty -m 'user-legacy-commits 2' --author="$other"
git commit --allow-empty -m 'user-legacy-commits 3' --author="$other"
git commit --allow-empty -m 'user-legacy-commits 4' --author="$user_legacy"     # !
git commit --allow-empty -m 'user-legacy-commits 5' --author="$other"

git checkout user-legacy2-commits
git commit --allow-empty -m 'user-legacy2-commits 1' --author="$other"
git commit --allow-empty -m 'user-legacy2-commits 2' --author="$other"
git commit --allow-empty -m 'user-legacy2-commits 3' --author="$other"
git commit --allow-empty -m 'user-legacy2-commits 4' --author="$user_legacy2"    # !
git commit --allow-empty -m 'user-legacy2-commits 5' --author="$other"

git checkout user-and-user-legacy-commits
git commit --allow-empty -m 'user-and-user-legacy-commits 1' --author="$other"
git commit --allow-empty -m 'user-and-user-legacy-commits 2' --author="$user"            # !
git commit --allow-empty -m 'user-and-user-legacy-commits 3' --author="$user_legacy"     # !
git commit --allow-empty -m 'user-and-user-legacy-commits 4' --author="$user_legacy2"    # !
git commit --allow-empty -m 'user-and-user-legacy-commits 5' --author="$other"

project_name='Alternative Email Addresses'
project_key='AEA'
create_project "$project_key" "$project_name" "$project_name"
set_project_permission "$project_key" 'PROJECT_WRITE'
create_repository "$project_key" "$repo_name"
push_repository "$project_key" '.' "$repo_name"


repo_name='branches-with-special-chars'
rm -rf "$repo_name"
mkdir "$repo_name"
cd "$repo_name"

author='admin <admin@example.com>'
git init
git commit --allow-empty -m 'initial' --author="$author"

# 20 branches
git checkout -b b_'"'
git checkout -b b_"'"
git checkout -b b_!
git checkout -b b_$
git checkout -b b_%
git checkout -b b_\(
git checkout -b b_\)
git checkout -b b_+
git checkout -b b_,
git checkout -b b_-
#git checkout -b "b_:"  fatal: 'b_:' is not a valid branch name.
git checkout -b "b_;"
git checkout -b "b_<"
git checkout -b b_=
git checkout -b "b_>"
git checkout -b b_@
git checkout -b b_]
#git checkout -b "b_^"  invaid
git checkout -b b_´
git checkout -b b_{
git checkout -b "b_|"
git checkout -b b_}
#git checkout -b b_~    invalid
git checkout -b b_"'""''"'"''""'"'"'"'  # Result is: b_'''"""'"

# 19 branches:
git checkout -b '"'
git checkout -b "'"
git checkout -b !
git checkout -b $
git checkout -b %
git checkout -b \(
git checkout -b \)
git checkout -b +
git checkout -b ,
#git checkout -b -  invalid
#git checkout -b :  fatal: ':' is not a valid branch name.
git checkout -b ";"
git checkout -b "<"
git checkout -b =
git checkout -b ">"
git checkout -b @
git checkout -b ]
git checkout -b ´
git checkout -b {
git checkout -b "|"
git checkout -b }

project_name='Branches With Special Chars'
project_key='BWSC'
create_project "$project_key" "$project_name" "$project_name"
set_project_permission "$project_key" 'PROJECT_WRITE'
create_repository "$project_key" "$repo_name"
push_repository "$project_key" '.' "$repo_name"


# Command line compatibility check
# cd /d/temp/1-10-commits-repo && git --version && git rev-list --first-parent --parents -n 1 --format='format: %h,%ad%n %an%n %ae%n %s' --date=local master
