#!/bin/bash

set -e
#set -x


#-----------
# Globals
#-----------
g_url_scheme='http'
g_bitbucket_url='localhost:7990/bitbucket'
g_user='admin'
g_password='admin'


#-----------
# Functions
#-----------


function curl_delete {
    local url="$1"

    curl -X DELETE -u 'admin:admin' "$g_url_scheme://$g_bitbucket_url/$url" >/dev/null
}

function delete_project {
    local projectKey="$1"

    curl_delete "rest/api/1.0/projects/$projectKey"
}

function delete_repo {
    local projectKey="$1"
    local repositorySlug="$2"

    curl_delete "rest/api/1.0/projects/$projectKey/repos/$repositorySlug"
}




#----------
# M A I N
#----------


repoPrefix='repo-'
noOfBranches=6
noOfProjects=2
noOfRepositories=2


repoWithCommitsOfAdminTemplateName='repoWithCommitsOfAdmin'
repositoryTemplateName="repo"


project1Key=""
repository1Name=""
set -x
for projectNo in `eval echo {1..$noOfProjects}`
do
    projectKey="P$(printf "%03d" $projectNo)"

    echo "Deleting repos of project $projectKey"

    for repositoryNo in `eval echo {1..$noOfRepositories}`
    do
        repoPostfix="-$projectKey-r$(printf "%04d" $repositoryNo)"
        repoName="${repositoryTemplateName}${repoPostfix}"
        repoWithCommitsOfAdminName="${repoWithCommitsOfAdminTemplateName}${repoPostfix}"

        echo "Deleting repo $repoName"
        delete_repo "$projectKey" "$repoName"

        if (( $repositoryNo == 1 ))
        then
            echo "Deleting repo $repoWithCommitsOfAdminName"
            delete_repo "$projectKey" "$repoWithCommitsOfAdminName"
        fi
    done

    echo "Deleting project $projectKey"
    delete_project "$projectKey"
done
