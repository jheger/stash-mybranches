#!/bin/bash

set -e
#set -x


#-----------
# Globals
#-----------
g_url_scheme='http'
g_bitbucket_url='localhost:7990/bitbucket'
g_user='admin'
g_password='admin'

g_userAdmin='Admin <admin@example.com>'
g_user1='User1 <user1@example.com>'


#-----------
# Functions
#-----------


# Convenience function for curl
function curl_post {
    local url="$1"
    local jsonData="$2"

    curl -H 'Content-type: application/json' -u 'admin:admin' --data "$jsonData" "$g_url_scheme://$g_bitbucket_url/$url" >/dev/null
}

function create_project {
    local key="$1"
    local name="$2"
    local description="$3"

    local data="{\"key\": \"$key\", \"name\": \"$name\", \"description\": \"$description\"}"
    curl_post 'rest/api/1.0/projects' "$data"
}

function set_project_permission {
    local projectKey="$1"
    local permission="$2"

    curl_post "rest/api/1.0/projects/$projectKey/permissions/$permission/all?allow=true"
}

function create_repository {
    local projectKey="$1"
    local name="$2"

    local data="{\"name\": \"$name\", \"scmId\": \"git\"}"
    curl_post "rest/api/1.0/projects/$projectKey/repos" "$data"
}

function push_repository {
    local projectKey="$1"
    local localRepositoryScmId="$2"
    local remoteRepositoryScmId="$3"

    cd "$localRepositoryScmId"
    git push --mirror "$g_url_scheme://$g_user:$g_password@$g_bitbucket_url/scm/$projectKey/$remoteRepositoryScmId" -q
    cd ..
}

function fork_repository {
    local fromProjectKey="$1"
    local fromRepositoryName="$2"
    local toProjectKey="$3"
    local toRepositoryName="$4"

    local data="{\"slug\": \"$toRepositoryName\",\"name\":\"$toRepositoryName\",\"project\":{\"key\":\"$toProjectKey\"}}"
    curl_post "rest/api/1.0/projects/$fromProjectKey/repos/$fromRepositoryName" "$data"
}

function checkout_branch_and_create_commits {
    local branchName=$1
    local count=$2

    git checkout -q -b $branchName
    for i in `eval echo {1..$count}`
    do
        git commit --allow-empty -q -m "commit $i on $branchName" --author="$g_user1"
    done
}

#----------
# M A I N
#----------

curl -u 'admin:admin' -v -X PUT -d "" -H "Content-Type: application/json" \
    "$g_url_scheme://$g_bitbucket_url/rest/api/latest/logs/logger/jh.atlassian.stash.mybranches/debug"


repoPrefix='repo-'
noOfBranches=6
noOfProjects=2
noOfRepositories=2


repoWithCommitsOfAdminTemplateName='repoWithCommitsOfAdmin'
rm -rf "$repoWithCommitsOfAdminTemplateName"
# Create one repo with commits of admin.
git init "$repoWithCommitsOfAdminTemplateName"
cd "$repoWithCommitsOfAdminTemplateName"
git commit --allow-empty -q -m "commit 1 on master" --author="$g_userAdmin"
git commit --allow-empty -q -m "commit 2 on master" --author="$g_userAdmin"
git commit --allow-empty -q -m "commit 3 on master" --author="$g_userAdmin"
git commit --allow-empty -q -m "commit 4 on master" --author="$g_userAdmin"
git commit --allow-empty -q -m "commit 5 on master" --author="$g_userAdmin"

cd ..

repositoryTemplateName="repo"
rm -rf "$repositoryTemplateName"
# Create master repo. This repo is going to be forked N times.
git init "$repositoryTemplateName"
cd "$repositoryTemplateName"
git commit --allow-empty -m "commit 1 on master" --author="$g_user1"
for branchNo in `eval echo {1..$noOfBranches}`
do
    git checkout -q master
    branchName="branch-$(printf "%03d" $branchNo)"
    echo "Creating branch $branchName"
    checkout_branch_and_create_commits "$branchName" 5
done
cd ..

project1Key=""
repository1Name=""

for projectNo in `eval echo {1..$noOfProjects}`
do
    projectKey="P$(printf "%03d" $projectNo)"
    echo "creating project $projectKey..."
    create_project "$projectKey" "Project-$projectKey" "Description of Project-$projectKey"

    set_project_permission "$projectKey" 'PROJECT_WRITE'

    for repositoryNo in `eval echo {1..$noOfRepositories}`
    do
        repoPostfix="-$projectKey-r$(printf "%04d" $repositoryNo)"
        repoName="${repositoryTemplateName}${repoPostfix}"
        repoWithCommitsOfAdminName="${repoWithCommitsOfAdminTemplateName}${repoPostfix}"

        # Push only one repository and fork the others. This is much more faster than pushing them all.
        if (( $projectNo == 1 && $repositoryNo == 1 ))
        then
            echo "creating repository $repoWithCommitsOfAdminName"
            create_repository "$projectKey" "$repoWithCommitsOfAdminName"
            echo "pushing repository $repoWithCommitsOfAdminName"
            push_repository "$projectKey" "$repoWithCommitsOfAdminTemplateName" "$repoWithCommitsOfAdminName"

            echo "creating repository $repoName"
            create_repository "$projectKey" "$repoName"
            echo "pushing repository $repoName"
            push_repository "$projectKey" "$repositoryTemplateName" "$repoName"

            project1Key="$projectKey"
            #repository1Name="$repositoryName"
            repo1Postfix="$repoPostfix"
        else
            echo "fork repository $project1Key/${repositoryTemplateName}${repo1Postfix} to $projectKey/$repoName"
            fork_repository "$project1Key" "${repositoryTemplateName}${repo1Postfix}" "$projectKey" "$repoName"

            if (($repositoryNo == 1 ))
            then
                echo "fork repository $project1Key/${repoWithCommitsOfAdminTemplateName}${repo1Postfix} to $projectKey/$repoWithCommitsOfAdminName"
                fork_repository "$project1Key" "${repoWithCommitsOfAdminTemplateName}${repo1Postfix}" "$projectKey" "$repoWithCommitsOfAdminName"
            fi
        fi
    done
done
